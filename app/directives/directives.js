/***
GLobal Directives
***/

// Route State Load Spinner(used on page or content load)
DigiboardApp.directive('ngSpinnerBar', ['$rootScope',
  function($rootScope) {
    return {
      link: function(scope, element, attrs) {
        // by defult hide the spinner bar
        element.addClass('hide'); // hide spinner bar by default

        // display the spinner bar whenever the route changes(the content part started loading)
        $rootScope.$on('$stateChangeStart', function() {
          element.removeClass('hide'); // show spinner bar
        });

        // hide the spinner bar on rounte change success(after the content loaded)
        $rootScope.$on('$stateChangeSuccess', function() {
          element.addClass('hide'); // hide spinner bar
          $('body').removeClass('page-on-load'); // remove page loading indicator
          // Layout.setSidebarMenuActiveLink('match'); // activate selected link in the sidebar menu

          // auto scorll to page top
          // setTimeout(function () {
          //   Digiboard.scrollTop(); // scroll to the top on content load
          // }, $rootScope.settings.layout.pageAutoScrollOnLoad);
        });

        // handle errors
        $rootScope.$on('$stateNotFound', function() {
          element.addClass('hide'); // hide spinner bar
        });

        // handle errors
        $rootScope.$on('$stateChangeError', function() {
          element.addClass('hide'); // hide spinner bar
        });
      }
    };
  }
])

// Handle global LINK click
DigiboardApp.directive('a', function() {
  return {
    restrict: 'E',
    link: function(scope, elem, attrs) {
      if (attrs.ngClick || attrs.href === '' || attrs.href === '#') {
        elem.on('click', function(e) {
          e.preventDefault(); // prevent link click for above criteria
        });
      }
    }
  };
});

// Handle Dropdown Hover Plugin Integration
DigiboardApp.directive('dropdownMenuHover', function () {
  return {
  link: function (scope, elem) {
    //elem.dropdownHover();
  }
  };
});

DigiboardApp.directive('ngConfirmClick', [
    function(){
      return {
        link: function (scope, element, attr) {
          var msg = attr.ngConfirmClick || "Are you sure?";
          var clickAction = attr.confirmedClick;
          element.bind('click',function (event) {
            if ( window.confirm(msg) ) {
              scope.$eval(clickAction)
            }
          });
        }
      };
  }]);

DigiboardApp.directive('datetime', function($compile) {
  return {
    restrict: 'E',
    scope: {
      ngModel: '=',
      ngChange: '&'
    },
    template: '<div class="datetime">' +
    '<datepicker show-weeks="false" ng-model="{{dtid}}[0]"></datepicker>' +
    '<timepicker defaultTime="5:00PM" style="margin:auto" ng-model="{{dtid}}[1]"></timepicker>' +
    '</div>',
    replace: true,
    terminal:true,
    priority:1000,
    link: function (scope, el, attrs, ngModelCtrl) {

      var dtid = 'dt_'+scope.$id;
      scope[dtid] = [];
      scope['dtid'] = dtid;

      var dp = jQuery('datepicker', el).attr('ng-model', dtid + '[0]'),
        tp = jQuery('timepicker', el).attr('ng-model', dtid + '[1]');

      $compile(dp)(scope);
      $compile(tp)(scope);

      scope.$watchCollection( dtid , function(newValue, oldValue, scope) {

        if (newValue[1] !== undefined && (newValue[1] !== oldValue[1] || newValue[0] !== oldValue[1])) {

          if (scope[dtid][0] === undefined)
            scope[dtid][0] = new Date();
          var date = moment(new Date(scope[dtid][0])).local().format('YYYY-MM-DD'),
            time = moment(new Date(scope[dtid][1])).local().format('HH:mmZ'),
            newDate = new Date(date + 'T' + time);
          newDate = new Date(moment(newDate).local());
          if (newDate > new Date()){
            scope.ngModel = newDate;
          }

          //scope.ngChange();
        }
      });

      var getNgModel = scope.$watch(function(){return scope.ngModel}, function(){
        var dateMoment = moment(new Date(scope.ngModel));

        if (scope.ngModel !== undefined && scope.ngModel !== null
          && moment().format('YYYY-MM-DDTHH:mm') !== dateMoment.format('YYYY-MM-DDTHH:mm')) {
          getNgModel();
          scope[dtid][0] = dateMoment.local().format();
          scope[dtid][1] = dateMoment.local().format();
        }
      });

    }

  }
});

DigiboardApp.directive('ob', function($rootScope) {
  return {
    restrict: 'AE',
    link: function(scope, element) {
      element.bind("click", function(e) {
        jQuery(document).orangeBox('create', jQuery(element));
      });
    }
  }
});

DigiboardApp.directive('ngDropzone', function ($rootScope) {
  return {
    restrict: 'AE',
    template: '<div style="display:none" ng-transclude></div>',
    transclude: true,
    scope: {
      dropzone: '=',
      dropzoneConfig: '=',
      eventHandlers: '='
    },
    link: function(scope, element, attrs, ctrls) {
      try { Dropzone }
      catch (error) {
        throw new Error('Dropzone.js not loaded.');
      }

      var dropzone = new Dropzone(element[0], $rootScope.dropzoneConfig);
      dropzone.on("addedfile", function(file) {
        $rootScope.dropzoneConfig.complete(file);
      });
      if (scope.eventHandlers) {
        Object.keys(scope.eventHandlers).forEach(function (eventName) {
          dropzone.on(eventName, scope.eventHandlers[eventName]);
        });
      }

      scope.dropzone = dropzone;
    }
  };
});

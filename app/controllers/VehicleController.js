'use strict';

DigiboardApp.controller('VehicleController', function($rootScope, $scope, Vehicle, $modal) {
    $scope.$on('$viewContentLoaded', function() {   
        // initialize core components
        //Digiboard.initAjax();
    });
    $scope.nVehicle = {};
    var getUsers = $rootScope.$watch(function() {
        return ($rootScope.users !== undefined && $rootScope.vehicles !== undefined && $rootScope.thisUser);
    }, function() {
        if ($rootScope.users !== undefined && $rootScope.vehicles !== undefined) {
            getUsers();
            Vehicle.linkAll();
            console.log('get vehicles');
            $rootScope.vehicles = Vehicle.getAll();
        }
    });
    $scope.saveUser = function(v) {
        if (v.user)
            v.user_id = v.user.id;
        v.DSSave();
    }
    $scope.createVehicle = function() {
        console.log($scope.nVehicle);
        if ($scope.nVehicle.user)
            $scope.nVehicle.user_id = $scope.nVehicle.user.id;

        Vehicle.create($scope.nVehicle).then(function(data){
            console.log(data);
            $scope.vEditMode = false;
            $scope.nVehicle = {};
            Vehicle.linkAll();
        });
    }

    $scope.deleteModal = function(data) {
        var modal = $modal.open({
            templateUrl: 'views/modals/deleteVehicle.html',
            controller: 'DeleteVehicleModalController',
            size:'sm',
            resolve: {
                vehicle: function() { return data; }
            }
        });
    };
});

DigiboardApp.controller('DeleteVehicleModalController', function($scope, $modalInstance, Vehicle, vehicle) {
    $scope.vehicle = vehicle;
    Digiboard.handleNoAutoZoomOnInput();

    $scope.cancel = function() {
        $modalInstance.dismiss('cancel');
    }

    $scope.destroy = function(vehicle_id) {
        if (typeof vehicle_id != 'undefined' && vehicle_id != '') {
            console.log('DELETING');
            Vehicle.destroy(vehicle_id).then(function(data) {
                $modalInstance.close({
                    vehicle: vehicle,
                    action: 'delete'
                });
            });
        }
    }
});

'use strict';

//
// This controller is used ONLY by the modal version (on "log in" button)
//  the login page ("/login") uses another controller with same name, but
//  on another app
//
var LoginController = DigiboardApp.controller('LoginController', [
  '$scope',
  '$rootScope',
  'User',
  'CheckinHistory',
  'Session',

  function(
    $scope,
    $rootScope,
    User,
    CheckinHistory,
    Session
  ) {

  $scope.login = {};
  $scope.error = "";
  $scope.isProcessing = false;

  var handleLogin = function(data) {
    if ($rootScope.thisUser) {
      $rootScope.thisUser = data;
      $rootScope.thisUser.live = User.get($rootScope.thisUser.user_id);
      Session.linkAll();
      CheckinHistory.find($rootScope.thisUser.user_id, { bypassCache: true }).then(function(history){
        CheckinHistory.linkAll();
        $rootScope.history = CheckinHistory.filter({where: {status: {'==' :  2}}});
        console.log(CheckinHistory);
      });
      var staffFilter = { where: { admin: {'!=' : true}, id: {'!=': $rootScope.thisUser.user_id}, inactive: {'!=': true}} };
      $rootScope.editUsers = ($rootScope.thisUser.user_id && !$rootScope.thisUser.admin) ? [$rootScope.thisUser.live] : [];
      $rootScope.staff = User.filter(staffFilter);
      $rootScope.editUsersMobile = $rootScope.thisUser.admin ? $rootScope.staff : [$rootScope.thisUser.live];
      console.log('user mobile');
      console.log($rootScope.editUsersMobile);
      $scope.ok();
    } else {
      window.location.href = "/";
    }

  }
  $rootScope.codeLogin = function() {
    User.login(undefined, undefined, $scope.login.code).then(function(data) {
      handleLogin(data);
    },function(data) {
      if (data.error.indexOf('Couldn\'t find User') !== -1) {
        $scope.error = "Incorrect login code";
      } else {
        $scope.error = data.error;
      }
      console.log(data);
    });
  }

  $rootScope.doLogin = function() {
    if ($scope.isProcessing) return ;

    $scope.isProcessing = true;
    User.login($scope.login.username, $scope.login.pass).then(function(data) {
      handleLogin(data);
    },function(data) {
      $scope.isProcessing = false;
      $scope.error = data.error;
    });
  }
}]);

'use strict';

DigiboardApp.controller('ReportsController', function($rootScope, $scope, $window, CheckinHistory) {
    $scope.$on('$viewContentLoaded', function() {   
        // initialize core components
        //Digiboard.initAjax();
    });
    $scope.report = {
        start_date: null,
        end_date: null
    };

    $scope.error = null;

    $scope.createReport = function() {
        if ($scope.report.start_date == null || 
            $scope.report.end_date == null ) {
            $scope.error = 'Start Date and End Date are required to generate a report'
        } else if ($scope.report.start_date > $scope.report.end_date){
            $scope.error = "Start Date should be less than end date to generate a report";
        } else {
            $scope.error = null;
            // CheckinHistory.generate_report($scope.report.start_date, 
            //     $scope.report.end_date);
            var url = window.api_url + "/checkin_histories/report.csv?start_date="+ 
            $scope.report.start_date + "&end_date=" + $scope.report.end_date;
            $window.open(url, '_blank');
        }
    }
});
DigiboardApp.controller('SchedulingController', ['$scope', '$rootScope', 'uiCalendarConfig', 'Checkin', 'Schedule',
  '$modal',
  function ( $scope, $rootScope, uiCalendarConfig, Checkin, Schedule, $modal ) {

  // enables all office locations
  $scope.offices_filter = {
    'blanked'  : true,
    'blanked': true,
    'blanked'  : true,
    'blanked'       : true,
    'Unassigned'    : true
  };

  // vehicle filter
  $scope.vehicle_filter = Array();

  //Inject fake event to prevent calendar from throwing errors.
  var date = new Date(), d = date.getDate(), m = date.getMonth(), y = date.getFullYear(),
    h = date.getHours(), min = date.getMinutes();
  if (!$scope.sources || !$scope.sources.length){
    $scope.sources = [{title: 'Inserting placeholder event as an event list promise.',start: new Date(2000, 1, 1)}];
    $scope.eventSources = [$scope.sources];
  }

  //
  $scope.refresh_calendar = function () {
    if (typeof($rootScope.vehicles) != 'object' ) {
      return ;
    }

    $rootScope.vehicles.forEach(function (vehicle) {
      var checked = $('input[name="v_'+ vehicle.id + '"]', '#vFilter').prop('checked');
      var office_visible = (
        ((typeof(vehicle.office) == 'undefined') && $scope.offices_filter['Unassigned']) ||
        ((typeof(vehicle.office) != 'undefined') && $scope.offices_filter[vehicle.office])
      );
      $(".fc-event.v_" + vehicle.id).toggle(checked && office_visible);
    });
  };

  $scope.of_all = function () {
    // enables all "Office filter" checkboxes and refresh calendar events
    for (var i in $scope.offices_filter) { $scope.offices_filter[i] = true };
    $scope.refresh_calendar();
  };

  $scope.of_none = function () {
    // disables all "Office filter" checkboxes and clear all calendar events
    for (var i in $scope.offices_filter) { $scope.offices_filter[i] = false };
    $scope.refresh_calendar();
  };

  $scope.vf_all = function () {
    // enables all "Vehicles filter" checkboxes and refresh calendar events
    $("#vFilter input").prop('checked', true);
    $rootScope.vehicles.forEach(function (vehicle) {
      $scope.vehicle_filter[vehicle.id] = true;
    });
    $scope.refresh_calendar();
  };

  $scope.vf_none = function () {
    // enables all "Vehicles filter" checkboxes and refresh calendar events
    $("#vFilter input").prop('checked', false);
    $rootScope.vehicles.forEach(function (vehicle) {
      $scope.vehicle_filter[vehicle.id] = false;
    });
    $scope.refresh_calendar();
  };

  $scope.schedulingEditModal = function(data) {
    var editor = $modal.open({
      templateUrl: 'views/modals/schedulingEdit.html',
      controller: 'SchedulingEditController',
      size:'md',
      resolve: {
        schedule: function() {
          if (data === undefined) {
            return {};
          }
          // if (data.classification !== undefined) {
            $scope.schedule = {
              vehicle_id: data.id,
              vehicle: data,
              start: new Date(moment().local().hour(7).minute(0).second(0).format()),
              end: new Date(moment().local().hour(17).minute(0).second(0).format())
            };
          // }

          if (data.start) {
            $scope.schedule = data;
          }

          return $scope.schedule;
        }
      }
    });

    editor.result.then(function(schedule) {
      if (schedule !== undefined && schedule.schedule !== undefined) {
        if (schedule.action == 'delete') {
          for(var i = 0; i < $scope.sources.length; i++ ) {
            if($scope.sources[i].id == schedule.schedule.id) {
              console.log("found deleting")
              $scope.sources.splice(i, 1);
            }
          }
        } else {
          //Updating calendar with object created by modal
          console.log(schedule);
          schedule.schedule.className = "v_" + schedule.schedule.vehicle_id;
          $scope.sources.push(schedule.schedule);
        }
      }
    });
  };

  $scope.dayClickEvent = function(day) {
    $scope.schedulingEditModal({
      start: new Date(moment(day).local().hour(8).minute(0).second(0).format()),
      end: new Date(moment(day).local().hour(17).minute(0).second(0).format())
    })
  }

  var updateCalendar = function() {
    var schedule = Schedule.getAll();
    console.log('update calendar');
    console.log($scope.sources);
    while ($scope.sources.length > 1) $scope.sources.pop();
    console.log($scope.sources);
    console.log(schedule);
    for(var key in schedule) {
      console.log(schedule[key]);
      schedule[key].className = "v_" + schedule[key].vehicle_id;
      $scope.sources.push(schedule[key]);
    }
  }
  // var calHeight = Digiboard.getViewPort().height -
  // $('.page-header').outerHeight(true) -
  // $('.page-footer').outerHeight(true) -
  // $('.page-title').outerHeight(true) -
  // $('.page-bar').outerHeight(true) - 107;

  // console.log(calHeight);

  $scope.uiConfig = {
    calendar:{
      // height: calHeight,
      editable: true,
      timezone: 'local',
      allDayDefault: false,
      header: {
        left: 'title',
        center: '',
        right: 'prev,next,today,month'//,agendaWeek,agendaDay'
      },
      eventClick: function( date, jsEvent, view){
        // console.log(date);
        $scope.schedulingEditModal(date);
      },
      eventDrop: function (date) {
        // console.log(date);
        Schedule.update(date.id, {schedule: date}).then(function(data) {
          // console.log(data);
        });
      },
      viewRender: function(view, element) {
        $scope.refresh_calendar();
      }
    }
  };

  var getSchedules = function() {
    Schedule.findAll().then(function(schedule){
      console.log(schedule)
      Schedule.linkAll();
      $scope.schedule = schedule;
      $scope.eventSources = [];
      $scope.sources.pop();

      $scope.scheduling_date = new Date();
      $scope.scheduling_date.setHours(0);
      $rootScope.scheduled = Schedule.scheduled(new Date($scope.scheduling_date.toString()));
      $rootScope.setTime = function() {
        $rootScope.scheduled = Schedule.scheduled(new Date($scope.scheduling_date));
      }
      $rootScope.is_available = function(value, index) {
        if ($rootScope.scheduled.length === 0) {
          $rootScope.scheduled = Schedule.scheduled(new Date($scope.scheduling_date));
        }
        for (var key in $rootScope.scheduled) {
          if ($rootScope.scheduled[key].vehicle_id == value.id) {
            return false;
          }
        }

        return true;
      }


      for(var key in schedule) {
//        } if (new Date(schedule[key].start) < new Date() && new Date(schedule[key].end) > new Date()) {
    var randcolor = parseInt(Math.random() * 255).toString(16) + parseInt(Math.random() * 255).toString(16) + parseInt(Math.random() * 255).toString(16);
        schedule[key].backgroundColor =  "#" + randcolor;
        schedule[key].className = "v_" + schedule[key].vehicle_id;

        $scope.sources.push(schedule[key]);
      }


      /*$scope.$watch(function () {
        return Schedule.lastModified();
      }, function () {
        //ui-calendar is buggy as hell.
         //while ($scope.sources.length > 1) $scope.sources.pop();
        var schedule = Schedule.getAll();

      });*/

    });
  }
  var getTitles = $rootScope.$watch(function() {
    return ($rootScope.users !== undefined && $rootScope.vehicles !== undefined);
  }, function() {
    if ($rootScope.users !== undefined && $rootScope.vehicles !== undefined) {
      getTitles();
      getSchedules();
    }
  });

}]);


DigiboardApp.controller('SchedulingEditController', [ '$rootScope', '$scope', '$modalInstance', 'schedule',
  'Schedule', 'Vehicle', 'User',
  function ( $rootScope, $scope, $modalInstance, schedule, Schedule, Vehicle, User ) {

  $scope.schedule = schedule;
  // Digiboard.handleNoAutoZoomOnInput();
  console.log(schedule);

  $scope.setVehicle = function() {
    Vehicle.find($scope.schedule.vehicle_id).then(function (vehicle) {
      $scope.schedule.vehicle = vehicle;
    });
  }

  $scope.setUser = function() {
    User.find($scope.schedule.user_id).then(function (user) {
      $scope.schedule.user = user;
    });
  }

  $scope.save = function() {
    console.log($scope.schedule);
    if (!$scope.schedule.vehicle) {
      $scope.schedule.vehicle_id = null;
    }

    if (!$scope.schedule.user) {
	  $scope.schedule.user_id = null;
    }


    if ($scope.schedule.id && (changes = Schedule.changes($scope.schedule.id))) {
      Schedule.update($scope.schedule.id, {schedule: $scope.schedule}).then(function(data) {
        //updateCalendar();
        $modalInstance.close({
          schedule: undefined
        });
      });
    } else {
      //create new
      console.log('CREATING');
      Schedule.create({schedule: $scope.schedule}).then(function(data){

        $modalInstance.close({
          schedule: data[0]
        });
      });
    }


  }

  $scope.cancel = function() {
    $modalInstance.dismiss('cancel');
  }

  $scope.destroy = function(schedule_id) {
    if (typeof schedule_id != 'undefined' && schedule_id != '') {
      console.log('DELETING');
      Schedule.destroy(schedule_id).then(function(data) {
        //updateCalendar();
        $modalInstance.close({
          schedule: schedule,
          action: 'delete'
        });
      });
    }
  }

  $scope.vehicle_mobiscroll_options = function (mobile)
  {
    if (typeof mobile == 'undefined') {
      mobile = false;
    }

    // extra options
    if (mobile == false) {
      extra = {
        anchor: '#bs-navbar-collapse',
        display: 'bubble',
        minWidth: 450
      };
    }
    else {
      extra = {
        display: 'top',
        mode: 'mixed'
      };
    }

    return $.extend(extra, {
      data: [],
      fullData: [],
      headerText: 'Vehicle',
      onInit: function (inst) {
        // current item list from "/vehicles" call
        var data = [{text: '- None -', value: ''}];
        $rootScope.vehicles.forEach(function (v) {
          // var html = '<div class="md-vehicle" data-name="'+ v.name +'"><div class="md-vehicle-name">' + v.name + '</div><div class="md-vehicle-details">' + v.model + ', ' + v.make + '</div></div>'
          var html = v.name;
          data.push({text: html, value: v.id})
        });
        inst.settings.data = data;
        inst.settings.fullData = data;
        inst.refresh();
      },
      onMarkupReady: function (markup, inst) {
        $('<div style="padding:.5em; color:black"><input id="autocomplete_filter" class="form-control input-lg" tabindex="0" placeholder="Type to filter..." /></div>')
          .prependTo($('.dwcc', markup))
          .on('keyup', function (e) {
            var query = $('input', this).val();
            var filtered = $.grep(inst.settings.fullData, function (val) {
              return (val.text + ', ' + val.value).match(new RegExp(query, 'ig'));
            });

            inst.settings.data = filtered.length ? filtered : [{text: 'No results', value: ''}];
            inst.refresh();
        });
      },
      onShow: function () {
        $('#autocomplete_filter').focus();
      },
      // onSelect: function (v, inst) {
      //   $(this).val($(v).data('name'));
      // },
    });
  };


  $scope.user_mobiscroll_options = function (mobile)
  {
    if (typeof mobile == 'undefined') {
      mobile = false;
    }

    // extra options
    if (mobile == false) {
      extra = {
        anchor: '#bs-navbar-collapse',
        display: 'bubble',
        minWidth: 450
      };
    }
    else {
      extra = {
        display: 'top',
        mode: 'mixed'
      };
    }

    return $.extend(extra, {
      data: [],
      fullData: [],
      headerText: 'User',
      onInit: function (inst) {
        // current item list from "/user" call
        var data = [{text: '- None -', value: ''}];
        $rootScope.users.forEach(function (v) {
          var html = v.name;
          data.push({text: html, value: v.id})
        });
        inst.settings.data = data;
        inst.settings.fullData = data;
        inst.refresh();
      },
      onMarkupReady: function (markup, inst) {
        $('<div style="padding:.5em; color:black"><input id="autocomplete_filter" class="form-control input-lg" tabindex="0" placeholder="Type to filter..." /></div>')
          .prependTo($('.dwcc', markup))
          .on('keyup', function (e) {
            var query = $('input', this).val();
            var filtered = $.grep(inst.settings.fullData, function (val) {
              return (val.text + ', ' + val.value).match(new RegExp(query, 'ig'));
            });

            inst.settings.data = filtered.length ? filtered : [{text: 'No results', value: ''}];
            inst.refresh();
        });
      },
      onShow: function () {
        $('#autocomplete_filter').focus();
      },
    });
  };

}]);

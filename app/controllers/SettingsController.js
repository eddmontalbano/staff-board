'use strict';

DigiboardApp.controller('SettingsController', function($rootScope, $scope, $http) {
    $scope.updating = false;
    $scope.update_staffDB = function() {
        $scope.updating = true;
        $http.get(window.api_url + '/update_staffDB').success(function(data) {
            $scope.updating = false;
            console.log('got here');
            if (data['error']) {
                addNotice(data['error'], 'danger');
            } else {
                addNotice('Success! '
                + data['users'] + ' new users, '
                + data['trucks'] + ' new trucks, '
                + data['projects'] + ' new projects. ')
            }
        }).error(function(err) {
            addNotice(err['error'], 'danger');
        });

    }

    function addNotice(msg, type) {
        if (type===undefined) type = "info";
        jQuery('#notices').html('<div class="note note-' + type + '">' + msg + '</div>')
    }



    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageBodySolid = true;
    $rootScope.settings.layout.pageSidebarClosed = false;
});

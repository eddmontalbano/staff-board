DigiboardApp.controller('CheckInModalController', function($scope, $rootScope, $timeout, $modalInstance, CheckinHistory) {
  $scope.user = $rootScope.user;

  // bug: mobiscroll doesn't understand 2015-05-15T14:20:00.000Z as a date
  $scope.user.checkin.return_datetime = new Date($scope.user.checkin.return_datetime);

  $scope.saveCheckin = function() {
    $rootScope.thisUser.checkin.status = 0;
    $rootScope.doCheckin(true);
    $modalInstance.close();
  }
  $scope.save = function() {
    $rootScope.thisUser.live.checkin.DSSave();
    $modalInstance.close();
  }

  $scope.cancel = function() {
    $modalInstance.dismiss('cancel');
  }

  $scope.$on('sliderEnd', function(slider) {
    var user = slider.targetScope.$parent.user;
    if (user.checkin.return_datetime && user.checkin.status !== 2) {
      $rootScope.doCheckin(false, null, user);
    }
    user.checkin.DSSave();
  });
});
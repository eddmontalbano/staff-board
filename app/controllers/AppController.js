DigiboardApp.controller('AppController', [ '$scope', '$rootScope', '$http', 'User', 'Vehicle', 'Project', 'Checkin',
  'Schedule', '$modal', '$timeout', 'CheckinHistory', 'Session', 'Dashboard', '$cookieStore',
  function ( $scope, $rootScope, $http, User, Vehicle, Project, Checkin, Schedule, $modal, $timeout, CheckinHistory,
             Session, Dashboard ) {

    if (! $rootScope.cookieContents) {
      window.location.href = '/login.html';
    }

    //
    // Updates local data from API server at app loading and every 5 minutes
    //
    var checkinsRefresher = function() {
      Dashboard.findAll({}, {bypassCache: true}).then(function(dashboard) {

        $rootScope.server_time = new Date(dashboard[0].server_time);
        $rootScope.server_time_diff = new Date() - $rootScope.server_time;

        User.inject(dashboard[0].users);
        Vehicle.inject(dashboard[0].vehicles);
        Checkin.inject(dashboard[0].checkins);
        Project.inject(dashboard[0].projects);

        var staffFilter = { where: { admin: {'!=' : true}, inactive: {'!=': true}} };
        $rootScope.vehicles = dashboard[0].vehicles;
        $rootScope.users = User.filter(staffFilter);
        $rootScope.checkins = dashboard[0].checkins;
        $rootScope.projects = dashboard[0].projects;
        // $rootScope.projects = Project.filter({where: { project_id: {'==' : null } } } );


        // $rootScope.users = User.filter(staffFilter);
        // $rootScope.staff = User.filter(staffFilter);
        User.findAll().then(function(users) {
          $rootScope.users = User.filter(staffFilter);
          $rootScope.staff = User.filter(staffFilter);
          Schedule.linkAll();

          var user_groups = angular.copy($rootScope.users);
          $rootScope.userGroups = {};
          for (var i=0; i<user_groups.length; i++) {
            var user = user_groups[i],
              group = user.name[0].toUpperCase();
            if ($rootScope.userGroups[group] == undefined) {
              $rootScope.userGroups[group] = [];
            }
            $rootScope.userGroups[group].push(user);
          }
        });

        Schedule.findAll().then(function(schedule) {
          Schedule.linkAll();
        });

        var vehicleFilter = (window.location.href.indexOf('/transportation') == -1) ? { where: { inactive: {'!=' : true} }} : {};
        $rootScope.vehicles = Vehicle.filter(vehicleFilter);
        // Vehicle.linkAll();
        // Vehicle.findAll().then(function(vehicles) {
        //   var vehicleFilter = (window.location.href.indexOf('/transportation') == -1) ? { where: { inactive: {'!=' : true} }} : {};
        //   $rootScope.vehicles = Vehicle.filter(vehicleFilter);
        // });

        $rootScope.projects = Project.filter({where: { project_id: {'==' : null } } } );
        // Project.findAll().then(function(data) {
        //   $rootScope.projects = Project.filter({where: { project_id: {'==' : null } } } );

        // $rootScope.projects = [];
        // data.forEach(function (p) {
        //   if (p.project_id == null) {
        //     $rootScope.projects.push(p);
        //   }
        // });

        // var project_groups = angular.copy($rootScope.projects);
        // $rootScope.projectGroups = {};
        // for (var i=0; i<project_groups.length; i++) {
        //   var prj = project_groups[i],
        //     group = prj.name[0].toUpperCase();
        //   if ($rootScope.projectGroups[group] == undefined) {
        //     $rootScope.projectGroups[group] = [];
        //   }
        //   $rootScope.projectGroups[group].push(prj);
        // }
        // });

        //
        // Get session data
        //
        if ($rootScope.cookieContents.session_id != null) {
          Session.find($rootScope.cookieContents.session_id, { bypassCache: true }).then(function (session)
          {
            //
            // Session data on rootScope
            //
            $rootScope.thisUser = session;
            $rootScope.thisUser.live = User.get($rootScope.thisUser.user_id);

            var staffFilter = { where: { admin: {'!=' : true}, id: {'!=': $rootScope.thisUser.user_id}, inactive: {'!=': true}} };
            $rootScope.staff = User.filter(staffFilter);

            // bug: mobiscroll doesn't understand 2015-05-15T14:20:00.000Z as a date
            $rootScope.thisUser.live.checkin.return_datetime =
              new Date($rootScope.thisUser.live.checkin.return_datetime);

            //
            // Set checkin list (readonly / editable)
            //
            $rootScope.editUsers = ($rootScope.thisUser.user_id && !$rootScope.thisUser.admin) ? [$rootScope.thisUser.live] : [];
            $rootScope.editUsersMobile = $rootScope.thisUser.admin ? $rootScope.staff : [$rootScope.thisUser.live];
          });
        }
      });

      $timeout(checkinsRefresher, 5000 * 60);
      // Project.findAll({}, {bypassCache: true}).then(function(data) {
      //   $rootScope.projects = Project.filter({where: { project_id: {'==' : null } } } );
      //   $timeout(checkinsRefresher, 5000 * 60);
      // });
    };
    checkinsRefresher();
  }]);
'use strict';

DigiboardApp.controller('CheckinController', function($rootScope, $scope, $interval, Checkin, $modal, Project, CheckinHistory, User, Vehicle, $http) {

  //
  // Clear "search" input on Checkin Board
  //
  $rootScope.staffSearch = '';

  //
  // Edit checkin: mobile mode
  //
  $rootScope.checkinEditMobile = function(user) {
    $rootScope.user = user;

    // IOS BUG: on iphone, modal scrolls background
    $('body').css('position','fixed');

    var checkout = $modal.open({
      templateUrl: 'views/checkinEditMobile.2.html',
      controller: 'CheckInModalController',
      size:'sm',
      dark:'dark',
    }).result.finally(function () {
      // IOS BUG: on iphone, modal scrolls background
      $('body').css('position','static');
    });

  }

  function defaultCheckinOrder(user) {
    if (! user) return '';
    if (! user.checkin) return '';
    if (user.checkin.status == 1) {
      return '5' + user.name;
    } else if (user.checkin.status == 0) {
      return '4' + user.name;
    }
    return user.checkin.status + user.name;
  }

  $scope.checkinOrder = defaultCheckinOrder;

  // clear search input every 5 min
  $interval(function () {
    $scope.clear_staff_search();
  }, 240 * 1000);

  $scope.getCheckinOrder = function() {
    return $scope.checkinOrder;
  };

  $scope.setCheckinOrder = function($column) {
    $scope.checkinOrder = $column;
  };

  $rootScope.minDate = function () {
    var now = new Date();
    return new Date(now.setTime(now.getTime() - 1000 * 60 * 1 - $rootScope.server_time_diff));
  };

  $rootScope.prevent_save_twice_bug = 0;
  $rootScope.saveDate = function(user) {
    if ($rootScope.prevent_save_twice_bug != 0) {
      $rootScope.prevent_save_twice_bug = 0;
      return ;
    }
    $rootScope.prevent_save_twice_bug += 1;

    user.checkin.DSSave().then(null, function (data) {
      user.checkin.return_datetime = new Date(data.data.return_datetime);
      alert(data.data.error);
    });
  }


  $rootScope.changeSlide = function(user) {
    console.log(user);
    if (user.checkin.status != 2 && user.checkin.status != 3) {
      $rootScope.doCheckin(false, null, user);
    }
    if (user.checkin.status == 1) {
      user.checkin.comment = undefined;
    } else if (user.checkin.status == 2) {
      console.log('set default time');
      var defaultTime = new Date();
      defaultTime.setHours(17);
      defaultTime.setMinutes(15);
      defaultTime.setSeconds(0);
      user.checkin.return_datetime = defaultTime;
    } else if (user.checkin.status == 3) {
      var defaultTime = new Date();
      defaultTime.setMinutes(defaultTime.getMinutes() + 60)
      user.checkin.return_datetime = defaultTime;
    }
    user.checkin.DSSave();
  }
  $scope.$on('sliderEnd', function(slider) {
    console.log(slider);
    var user = slider.targetScope.$parent.$parent.user;
    $rootScope.changeSlide(user);
  });

  $rootScope.fromLogin = false;

  $scope.clear_staff_search = function ()
  {
    setTimeout(function () {
      window.staff_search_input.clear();
      $('#staff-search input').not('.form-control').val('');
      $scope.checkinOrder = defaultCheckinOrder;
    }, 100);
  };

  $rootScope.staff_search_mobiscroll_options = function (mobile)
  {
    if (typeof mobile == 'undefined') {
      mobile = false;
    }

    // extra options
    if (mobile == false) {
      extra = {
        anchor: '#bs-navbar-collapse',
        display: 'bubble',
        minWidth: 450
      };
    }
    else {
      extra = {
        display: 'top',
        mode: 'mixed'
      };
    }

    return $.extend(extra, {
      data: [],
      fullData: [],
      placeholder: 'search...',
      headerText: 'Staff',
      buttons: ['set', 'clear', 'cancel'],
      onInit: function (inst) {
        window.staff_search_input = inst;
      },
      onBeforeShow: function (inst) {
        // var data = [];
        var data = [{text: '', value: ''}];
        $rootScope.staff.forEach(function (v) {
          data.push({text: v.name, value: v.name})
        });
        inst.settings.data = data;
        inst.settings.fullData = data;
        inst.refresh();
      },
      onMarkupReady: function (markup, inst) {
        $('<div style="padding:.5em; color:black"><input id="autocomplete_filter" class="form-control input-lg" tabindex="0" placeholder="Type to filter..." /></div>')
          .prependTo($('.dwcc', markup))
          .on('keyup', function (e) {
            var query = $('input', this).val();
            var filtered = $.grep(inst.settings.fullData, function (val) {
              return (val.text + ', ' + val.value).match(new RegExp(query, 'ig'));
            });

            inst.settings.data = filtered.length ? filtered : [{text: 'No results', value: ''}];
            inst.refresh();
        });
      },
      onShow: function () {
        $('#autocomplete_filter').focus();
      },
    });
  };

  $rootScope.office_location_mobiscroll_options = function (mobile)
  {
    if (typeof mobile == 'undefined') {
      mobile = false;
    }

    // extra options
    if (mobile == false) {
      extra = {
        anchor: '#bs-navbar-collapse',
        display: 'bubble',
        minWidth: 450
      };
    }
    else {
      extra = {
        display: 'top',
        mode: 'mixed'
      };
    }

    // item list
    var data = [
      {text: 'blanked', value: 'blanked'},
      {text: 'blanked', value: 'blanked'},
      {text: 'blanked', value: 'blanked'},
      {text: 'blanked', value: 'blanked'},
    ];

    return $.extend(extra, {
      data: data,
      headerText: 'Location',
    });
  };

  $rootScope.status_mobiscroll_options = function (mobile)
  {
    if (typeof mobile == 'undefined') {
      mobile = false;
    }

    // extra options
    if (mobile == false) {
      extra = {
        anchor: '#bs-navbar-collapse',
        display: 'bubble',
        minWidth: 450
      };
    }
    else {
      extra = {
        display: 'top',
        mode: 'mixed',
      };
    }

    // item list
    var data = [
      {text: 'Office', value: '0'},
      {text: 'Off', value: '1'},
      {text: 'Field', value: '2'},
      {text: 'Away', value: '3'},
    ];

    return $.extend(extra, {
      data: data,
      headerText: 'Status',
    });
  };

  $rootScope.location_mobiscroll_options = function (item, headerText, checkin, mobile)
  {
    if (typeof mobile == 'undefined') {
      mobile = false;
    }

    // extra options
    if (mobile == false) {
      extra = {
        anchor: '#bs-navbar-collapse',
        display: 'bubble',
        minWidth: 450
      };
    }
    else {
      extra = {
        display: 'top',
        mode: 'mixed'
      };
    }

    return $.extend(extra, {
      data: [],
      fullData: [],
      cacheLastParent: null,
      headerText: headerText,
      onInit: function (inst) {
        // add custom functions
        inst.getItemParent = function () {
          itemParent = null;
          switch (headerText) {
            case 'Phase': itemParent = checkin.project; break;
            case 'Subphase': itemParent = checkin.phase; break;
            case 'Sub-subphase': itemParent = checkin.subphase; break;
          }
          return itemParent;
        };

        // current (and probably incomplete) item list from "/projects" call
        var data = [{text: '- ' + headerText + ' -', value: ''}];
        var itemParent = inst.getItemParent();
        if (itemParent) {
          itemParent.projects.forEach(function (v) {
            data.push({text: v.name, value: v.id})
          });
        }
        inst.settings.data = data;
        inst.settings.fullData = data;
        inst.refresh();
      },
      onMarkupReady: function (markup, inst) {
        $('<div style="padding:.5em; color:black"><input id="autocomplete_filter" class="form-control input-lg" tabindex="0" placeholder="Type to filter..." /></div>')
          .prependTo($('.dwcc', markup))
          .on('keyup', function (e) {
            var query = $('input', this).val();
            var filtered = $.grep(inst.settings.fullData, function (val) {
              return (val.text + ', ' + val.value).match(new RegExp(query, 'ig'));
            });

            inst.settings.data = filtered.length ? filtered : [{text: 'No results', value: ''}];
            inst.refresh();
        });
      },
      onBeforeShow: function (inst) {
        var itemParent = inst.getItemParent();

        // cache-hit
        if (! itemParent) return ;
        if (inst.settings.cacheLastParent) {
          if (inst.settings.cacheLastParent.id == itemParent.id) return ;
        }

        getProjectLinks(itemParent.id, function () {
          var options = [{text: '- ' + headerText + ' -', value: ''}];
          itemParent.projects.forEach(function (v) {
            options.push({text: v.name, value: v.id})
          });
          inst.settings.fullData = options;
          inst.settings.data = options;
          inst.settings.cacheLastParent = itemParent;
          inst.refresh();
        });
      },
      onShow: function () {
        $('#autocomplete_filter').focus();
      },
      onSelect: function (v, inst) {
        $rootScope.checkLinks(checkin);
      },
    });
  };

  $rootScope.vehicle_mobiscroll_options = function (mobile)
  {
    if (typeof mobile == 'undefined') {
      mobile = false;
    }

    // extra options
    if (mobile == false) {
      extra = {
        anchor: '#bs-navbar-collapse',
        display: 'bubble',
        minWidth: 450
      };
    }
    else {
      extra = {
        display: 'top',
        mode: 'mixed'
      };
    }

    return $.extend(extra, {
      data: [],
      fullData: [],
      headerText: 'Vehicle',
      onInit: function (inst) {
        // current item list from "/vehicles" call
        var data = [{text: '- Vehicle -', value: ''}];
        $rootScope.vehicles.forEach(function (v) {
          // var html = '<div class="md-vehicle" data-name="'+ v.name +'"><div class="md-vehicle-name">' + v.name + '</div><div class="md-vehicle-details">' + v.model + ', ' + v.make + '</div></div>'
          var html = v.name;
          data.push({text: html, value: v.id})
        });
        inst.settings.data = data;
        inst.settings.fullData = data;
        inst.refresh();
      },
      onMarkupReady: function (markup, inst) {
        $('<div style="padding:.5em; color:black"><input id="autocomplete_filter" class="form-control input-lg" tabindex="0" placeholder="Type to filter..." /></div>')
          .prependTo($('.dwcc', markup))
          .on('keyup', function (e) {
            var query = $('input', this).val();
            var filtered = $.grep(inst.settings.fullData, function (val) {
              return (val.text + ', ' + val.value).match(new RegExp(query, 'ig'));
            });

            inst.settings.data = filtered.length ? filtered : [{text: 'No results', value: ''}];
            inst.refresh();
        });
      },
      onShow: function () {
        $('#autocomplete_filter').focus();
      }
      // onSelect: function (v, inst) {
      //   $(this).val($(v).data('name'));
      // },
    });
  };

  $rootScope.user_mobiscroll_options = function (mobile)
  {
    if (typeof mobile == 'undefined') {
      mobile = false;
    }

    // extra options
    if (mobile == false) {
      extra = {
        anchor: '#bs-navbar-collapse',
        display: 'bubble',
        minWidth: 450
      };
    }
    else {
      extra = {
        display: 'top',
        mode: 'mixed'
      };
    }

    return $.extend(extra, {
      data: [],
      fullData: [],
      headerText: 'User',
      onInit: function (inst) {
        // current item list from "/user" call
        var data = [{text: '- None -', value: ''}];
        $rootScope.users.forEach(function (v) {
          var html = v.name;
          data.push({text: html, value: v.id})
        });
        inst.settings.data = data;
        inst.settings.fullData = data;
        inst.refresh();
      },
      onMarkupReady: function (markup, inst) {
        $('<div style="padding:.5em; color:black"><input id="autocomplete_filter" class="form-control input-lg" tabindex="0" placeholder="Type to filter..." /></div>')
          .prependTo($('.dwcc', markup))
          .on('keyup', function (e) {
            var query = $('input', this).val();
            var filtered = $.grep(inst.settings.fullData, function (val) {
              return (val.text + ', ' + val.value).match(new RegExp(query, 'ig'));
            });

            inst.settings.data = filtered.length ? filtered : [{text: 'No results', value: ''}];
            inst.refresh();
        });
      },
      onShow: function () {
        $('#autocomplete_filter').focus();
      },
    });
  };

  $rootScope.project_mobiscroll_options = function (projectGroups, mobile)
  {
    if (typeof mobile == 'undefined') {
      mobile = false;
    }

    // extra options
    if (mobile == false) {
      extra = {
        anchor: '#bs-navbar-collapse',
        display: 'bubble',
        minWidth: 400
      };
    }
    else {
      extra = {
        display: 'top',
        mode: 'mixed'
      };
    }

    return $.extend(extra, {
      data: [],
      fullData: [],
      headerText: 'Project',
      minWidth: 450,
      onInit: function (inst) {
        var data = [{text: '- Project -', value: ''}];
        $rootScope.projects.forEach(function (v) {
          data.push({text: v.name, value: v.id});
        });
        inst.settings.data = data;
        inst.settings.fullData = data;
        inst.refresh();
      },
      onMarkupReady: function (markup, inst) {
        $('<div style="padding:.5em; color:black"><input id="autocomplete_filter" class="form-control input-lg" tabindex="0" placeholder="Type to filter..." /></div>')
          .prependTo($('.dwcc', markup))
          .on('keyup', function (e) {
            var query = $('input', this).val();
            var filtered = $.grep(inst.settings.fullData, function (val) {
              return (val.text + ', ' + val.value).match(new RegExp(query, 'ig'));
            });

            inst.settings.data = filtered.length ? filtered : [{text: 'No results', value: ''}];
            inst.refresh();
        });
      },
      onShow: function () {
        $('#autocomplete_filter').focus();
      },
    });
  };

  function getProjectLinks(prj_id, callback) {
    Project.find(prj_id, {bypassCache: true}).then(function() {
      Project.linkAll();

      if (typeof callback !== 'undefined') {
        callback();
      }
    });
  }

  $rootScope.doCheckin = function(confirm, selector, user) {
    var checkin = null;
    if (user === undefined)
      checkin = $rootScope.thisUser.live.checkin;
    else
      checkin = user.checkin;

    checkin.project_id = checkin.return_datetime = checkin.vehicle_id = checkin.phase_id = checkin.subphase_id =
      checkin.travel_method = checkin.project = checkin.subphase = checkin.phase = checkin.vehicle = checkin.comment = null;

    if ($rootScope.checkLinks !== undefined)
      $rootScope.checkLinks(user);

    user.DSSave();
  };

  $rootScope.checkLinks = function (checkin) {
    var old_project_id = checkin.project ? checkin.project.id : null;
    var old_phase_id = checkin.phase ? checkin.phase.id : null;
    var old_subphase_id = checkin.subphase ? checkin.subphase.id : null;
    var old_subsub_id = checkin.subsub ? checkin.subsub.id : null;

    checkin.project = checkin.project_id ? Project.get(checkin.project_id) : null;
    checkin.phase = checkin.phase_id ? Project.get(checkin.phase_id) : null;
    checkin.subphase = checkin.subphase_id ? Project.get(checkin.subphase_id) : null;
    checkin.subsub = checkin.subsub_id ? Project.get(checkin.subsub_id) : null;

    // project changed
    if (old_project_id != checkin.project_id) {
      getProjectLinks(checkin.project_id);
      checkin.phase = null;
      checkin.subphase = null;
      checkin.subsub = null;
      checkin.phase_id = null;
      checkin.subphase_id = null;
      checkin.subsub_id = null;
    }

    // phase changed
    if (old_phase_id != checkin.phase_id) {
      getProjectLinks(checkin.phase_id);
      checkin.subphase = null;
      checkin.subsub = null;
      checkin.subphase_id = null;
      checkin.subsub_id = null;
    }

    // subphase changed
    if (old_subphase_id != checkin.subphase_id) {
      getProjectLinks(checkin.subphase_id);
      checkin.subsub = null;
      checkin.subsub_id = null;
    }

    // save
    checkin.DSSave();
  }

  var dropzone;
  $rootScope.dropzoneConfig = {
    url: "/projects/0/attach",
    dictDefaultMessage: "Drop ERP here or click to upload",
    acceptedFiles: 'application/pdf'
  }

  // "copy" button on checkins board
  $scope.copyCheckin = function (user, date) {
    $http.get(window.api_url + '/users/' + user.id + '/checkin-by-date/' + date.toISOString().substr(0, 10)).
      success(function(data, status, headers, config) {
        if (data == null) {
          alert('No checkins on ' + $.datepicker.formatDate( "M d, yy",new Date(date)));
        }
        else {
          var now = new Date();
          user.checkin.status = data.status;
          user.checkin.return_datetime = new Date(now.toISOString().substr(0, 10) + data.return_datetime.substr(10));
          user.checkin.status = data.status;
          user.checkin.office = data.office;
          user.checkin.vehicle_id = data.vehicle_id;
          user.checkin.vehicle = !data.vehicle_id ? null : Vehicle.get(data.vehicle_id);
          user.checkin.comment = data.comment;

          // load projects
          Project.inject(
            [data.phase, data.subphase, data.subsub].
            concat(data.projects).
            filter(function(n){ return n != undefined })
          );
          user.checkin.project_id = data.project_id;
          user.checkin.project = !data.project_id ? null : Project.get(data.project_id);
          user.checkin.phase_id = data.phase_id;
          user.checkin.phase = !data.phase_id ? null : Project.get(data.phase_id);
          user.checkin.subphase_id = data.subphase_id;
          user.checkin.subphase = !data.subphase_id ? null : Project.get(data.subphase_id);
          user.checkin.subsub_id = data.subsub_id;
          user.checkin.subsub = !data.subsub_id ? null : Project.get(data.subsub_id);

          // save
          user.checkin.DSSave();
        }
      }).
      error(function(data, status, headers, config) {
        alert('error');
        console.log(data);
      });
  };

  $scope.editUser = function (user) {
    // bug: mobiscroll doesn't understand 2015-05-15T14:20:00.000Z as a date
    user.checkin.return_datetime = new Date(user.checkin.return_datetime);

    var index = $scope.staff.indexOf(user);
    $scope.staff.splice(index, 1);
    $scope.editUsers.push(user);
  };

  $scope.attachERP = function(project_id) {
    $rootScope.dropzoneConfig.url = window.api_url + "/projects/"+ parseInt(project_id)+"/attach";
    $rootScope.dropzoneConfig.complete = function(file) {
      if (file.status =="error") {
        alert(file.xhr.responseText);
      }
      Project.find(parseInt(project_id), {bypassCache:true});

      // dropzone.dismiss();
    }
    dropzone = $modal.open({
      templateUrl: 'views/modals/erpUpload.html',
      controller: 'ErpModalController',
      resolve: {
        project_id: function(){ return project_id }
      }
    });

    $rootScope.dropzoneConfig.uploadprogress = function(file, progress, bytesSent) {
      var node, _i, _len, _ref, _results;
      if (file.previewElement) {
        _ref = file.previewElement.querySelectorAll("[data-dz-uploadprogress]");
        _results = [];
        for (_i = 0, _len = _ref.length; _i < _len; _i++) {
        node = _ref[_i];
        if (node.nodeName === 'PROGRESS') {
          _results.push(node.value = progress);
        } else {
          _results.push(node.style.width = "" + progress + "%");
        }
        }
        return _results;
      }
    };
    $rootScope.dropzoneConfig.success = function(file) {
        dropzone.dismiss();
    };
  };

  $scope.deleteERP = function (user) {
    var project = $scope.projectErp(user);
    if (project) {
      var url = window.api_url + "/users/" + user.id + "/erp-attachment.pdf/delete";
      $http.get(url).then(function (response) {
        if (response) {
          // Project.find(parseInt(project.project_id), {bypassCache:true});
          project.erp_path = null;
        }
      });
    }
  };

  $scope.projectErp = function(user) {
    if (user !== undefined && user.checkin !== undefined) {
      if (user.checkin.subsub) {
        return user.checkin.subsub;
      } else if (user.checkin.subphase) {
        return user.checkin.subphase;
      } else if (user.checkin.phase) {
        return user.checkin.phase;
      } else if (user.checkin.project) {
        return user.checkin.project;
      }
    }

    return {erp_path: false};
  }

  // set sidebar closed and body solid layout mode
  $rootScope.settings.layout.pageBodySolid = true;
  $rootScope.settings.layout.pageSidebarClosed = false;
});


DigiboardApp.controller('ErpModalController', function($scope, $modalInstance) {

});


DigiboardApp.directive("confirmButton", function($document, $parse) {
  return {
    restrict: 'A',
    link: function(scope, element, attrs) {
      var buttonId, html, message, nope, title, yep;

      buttonId = Math.floor(Math.random() * 10000000000);

      attrs.buttonId = buttonId;

      message = attrs.message || "Are you sure?";
      yep = attrs.yes || "Yes";
      nope = attrs.no || "No";
      title = attrs.title || "Confirm";

      html = "<div id=\"button-" + buttonId + "\">\n  <span class=\"confirmbutton-msg\">" + message + "</span><br>\n  <button class=\"confirmbutton-yes btn btn-danger\">" + yep + "</button>\n   <button class=\"confirmbutton-no btn\">" + nope + "</button>\n</div>";

      element.popover({
        content: html,
        html: true,
        trigger: "manual",
        title: title
      });

      return element.bind('click', function(e) {
        var dontBubble, pop;
        dontBubble = true;

        e.stopPropagation();

        element.popover('show');

        pop = $("#button-" + buttonId);

        pop.closest(".popover").click(function(e) {
          if (dontBubble) {
            e.stopPropagation();
          }
        });

        pop.find('.confirmbutton-yes').click(function(e) {
          dontBubble = false;

          var func = $parse(attrs.confirmButton);
          func(scope);
        });

        pop.find('.confirmbutton-no').click(function(e) {
          dontBubble = false;

          $document.off('click.confirmbutton.' + buttonId);

          element.popover('hide');
        });

        $document.on('click.confirmbutton.' + buttonId, ":not(.popover, .popover *)", function() {
          $document.off('click.confirmbutton.' + buttonId);
          element.popover('hide');
        });
      });
    }
  };
});

'use strict';

DigiboardApp.controller('StaffController', function($rootScope, $scope, User) {
    $scope.$on('$viewContentLoaded', function() {
        // initialize core components
        //Digiboard.initAjax();
    });
    $scope.nUser = {};

    $scope.save = function (user) {
      user.DSSave();
      user.backup_supervisor = user.backup_supervisor_id ? User.get(user.backup_supervisor_id) : null;
    }

    $scope.createUser = function() {
        User.create($scope.nUser).then(function(data){
            console.log(data);
            $scope.vEditMode = false;
            $scope.nUser = {};
        });
    }

    $scope.deleteUser = function(user) {
        if (confirm('Are you sure you want to delete ' + user.name + ' from the system? This is not reversible.')) {
            User.destroy(user.id);
        }
    }

    $scope.user_mobiscroll_options = function (mobile)
    {
      if (typeof mobile == 'undefined') {
        mobile = false;
      }

      // extra options
      if (mobile == false) {
        extra = {
          anchor: '#bs-navbar-collapse',
          display: 'bubble',
          minWidth: 450
        };
      }
      else {
        extra = {
          display: 'top',
          mode: 'mixed'
        };
      }

      return $.extend(extra, {
        data: [],
        fullData: [],
        headerText: 'User',
        buttons: ['set', 'clear', 'cancel'],
        onInit: function (inst) {
          $rootScope.$watch(function() {
            return ($rootScope.users !== undefined);
          }, function() {
            if ($rootScope.users !== undefined) {
              // current item list
              var data = [{text: '- None -', value: ''}];
              $rootScope.users.forEach(function (v) {
                var html = v.name;
                data.push({text: html, value: v.id})
              });
              inst.settings.data = data;
              inst.settings.fullData = data;
              inst.refresh();
            }
          });
        },
        onMarkupReady: function (markup, inst) {
          $('<div style="padding:.5em; color:black"><input id="autocomplete_filter" class="form-control input-lg" tabindex="0" placeholder="Type to filter..." /></div>')
            .prependTo($('.dwcc', markup))
            .on('keyup', function (e) {
              var query = $('input', this).val();
              var filtered = $.grep(inst.settings.fullData, function (val) {
                return (val.text + ', ' + val.value).match(new RegExp(query, 'ig'));
              });

              inst.settings.data = filtered.length ? filtered : [{text: 'No results', value: ''}];
              inst.refresh();
          });
        },
        onShow: function () {
          $('#autocomplete_filter').focus();
        },
      });
    };

    var fullUserlist = $rootScope.$watch(function() {
        return ($rootScope.users !== undefined);
    }, function() {
        if ($rootScope.users !== undefined) {
            fullUserlist();
            $rootScope.users = User.filter();
        }
    });
});

// mobiscroll user
DigiboardApp.directive('editableUser', ['editableDirectiveFactory',
  function(editableDirectiveFactory) {
    return editableDirectiveFactory({
      directiveName: 'editableUser',
      inputTpl: '<input mobiscroll-select="user_mobiscroll_options()" />',
      // render: function() {
      //   this.parent.render.call(this);
      //   var div = angular.element('<div style="padding-right: 74px !important;"></div>');
      //   this.inputEl.wrap(div);
      // },
      autosubmit: function() {
        var self = this;
        self.inputEl.bind('change', function() {
          self.scope.$apply(function() {
            self.scope.$form.$submit();
          });
        });
      }
    });
}]);

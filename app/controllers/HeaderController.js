//
// This controller is used by the header and it's present at all pages
//   Use this controller to put header's elements related code
//   e.g.: "reload" button, "back to the top" button, login, logout, lock
//
DigiboardApp.controller('HeaderController', [
  '$rootScope',
  '$scope',
  '$cookieStore',
  '$http',
  '$modal',
  '$timeout',
  'Session',
  'User',
  'CheckinHistory',

  function (
    $rootScope,
    $scope,
    $cookieStore,
    $http,
    $modal,
    $timeout,
    Session,
    User,
    CheckinHistory
  ) {

  //
  // "Back to the top" button: hide / show "Back to the top" button
  //    while scrolling
  //
  jQuery(window).scroll(function() {
    var offset = 250;
    var duration = 50;
    if (jQuery(this).scrollTop() > offset) {
      jQuery('.back-to-top').fadeIn(duration);
    } else {
      jQuery('.back-to-top').fadeOut(duration);
    }
  });

  //
  // "Back to the top" button: click on "Back to the top"
  //
  $scope.back_to_top = function() {
    var duration = 50;
    jQuery('html, body').animate({scrollTop: 0}, duration);
  };

  //
  // "Reload" button
  //
  $scope.reload_and_clear_cache = function() {
    location.reload(true);
  };

  //
  // "Lock" button
  //
  $scope.lock = function() {
    // lock this session on api server and redirects to login page
    $http.get(window.api_url + '/lock').then(function() {
      $rootScope.thisUser = null; // setting this to null, so the app will not show the board (not even in read-only)
      window.location.href = '/login.html';
    });
  };

  //
  // "Logout" button
  //
  $rootScope.logout = function() {
    $rootScope.fromLogin = false;
    var staffFilter = { where: { admin: {'!=' : true}, inactive: {'!=': true}} };
    $rootScope.editUsers = [];
    $rootScope.staff = User.filter(staffFilter);
    $rootScope.editUsersMobile = [];
    $http.get(window.api_url + '/logout').then(function() {
      try {
        // var cookieName = 'digitalboard_user';
        // $cookieStore.remove(cookieName);
        $rootScope.thisUser.user_id = null;
        $rootScope.thisUser.admin = null;

        $rootScope.cookieContents.session_id = null;
        $rootScope.cookieContents.user_id = null;
        $rootScope.cookieContents.admin = null;

        var now = new Date();
        var exp = new Date(now.getFullYear(), now.getMonth() + 6, now.getDate());
        $cookieStore.put(
          $rootScope.cookieName,
          $rootScope.cookieContents,
          {expires: exp}
        );
      } catch (e) {
        // $rootScope.thisUser = null;
        window.location.href = '/';
      }
    });
  };

  $rootScope.loginModal = function(callback) {
    jQuery('html, body').animate({scrollTop: 0}, 50);
    LoginController['$inject'] = ['$modalInstance'];
    $rootScope.thisUser = true;
    var loginBox = $modal.open({
      templateUrl: 'views/login.html',
      controller: 'LoginController',
      size:'sm',
      scope: $scope,
    });

    loginBox.opened.then(function() {
      $timeout(function() {$('[name="code"]').focus()}, 100);
    });

    loginBox.result.then(function(doLogin) {
      if (callback !== undefined)
        callback.call();
    });

    $scope.ok = function () {
      loginBox.close();
    };
  };

}]);

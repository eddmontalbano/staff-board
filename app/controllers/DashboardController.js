'use strict';

DigiboardApp.controller('DashboardController', function($rootScope, $scope, $http, $timeout) {
    // $scope.$on('$viewContentLoaded', function() {   
    //     // initialize core components
    //     Digiboard.initAjax();
    // });

    // set sidebar closed and body solid layout mode
    $rootScope.settings.layout.pageBodySolid = true;
    $rootScope.settings.layout.pageSidebarClosed = false;
});
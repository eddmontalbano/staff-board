module.exports = function(grunt) {
grunt.loadNpmTasks('grunt-contrib-uglify');
grunt.loadNpmTasks('grunt-contrib-cssmin');
grunt.loadNpmTasks('grunt-cache-breaker');
grunt.loadNpmTasks('grunt-concurrent');

// Project configuration.
grunt.initConfig({
  uglify: {
    options: {
      mangle: false,
      compress: false,
      sourceMap: false
    },
    app: {
      files: {
        'assets/javascripts/all-login.min.js': [
          'vendor/bower_components/jquery/dist/jquery.js',
          'vendor/bower_components/bootstrap/dist/js/bootstrap.js',
          'vendor/bower_components/angular/angular.js',
          'vendor/bower_components/angular-cookies/angular-cookies.js',
          'vendor/bower_components/ui-router/release/angular-ui-router.js',
          'assets/javascripts/app-login.js',
        ],

        'assets/javascripts/all.min.js': [
          'vendor/bower_components/jquery/dist/jquery.js',
          'vendor/bower_components/angular/angular.js',
          'vendor/jquery-slimscroll.js',
          'vendor/bower_components/bootstrap/dist/js/bootstrap.js',
          'vendor/bower_components/bootstrap-switch/dist/js/bootstrap-switch.js',
          'vendor/bower_components/angular-resource/angular-resource.js',
          'vendor/bower_components/angular-cookies/angular-cookies.js',
          'vendor/bower_components/angular-bootstrap/ui-bootstrap-tpls.js',
          'vendor/bower_components/angular-bootstrap-switch/dist/angular-bootstrap-switch.js',
          'vendor/bower_components/angular-tablesort/js/angular-tablesort.js',
          'vendor/bower_components/angular-ui-slider/src/slider.js',
          'vendor/bower_components/ui-router/release/angular-ui-router.js',
          'vendor/bower_components/angular-sanitize/angular-sanitize.js',
          'vendor/bower_components/angular-touch/angular-touch.js',
          'vendor/bower_components/angular-loading-bar/build/loading-bar.js',
          'vendor/bower_components/angular-xeditable/dist/js/xeditable.js',
          'vendor/bower_components/angular-dragdrop/js/angular-dragdrop.js',
          'vendor/bower_components/dropzone/dist/dropzone.js',
          'vendor/bower_components/moment/moment.js',
          'vendor/bower_components/js-data/dist/js-data.js',
          'vendor/bower_components/js-data-angular/dist/js-data-angular.js',
          'vendor/bower_components/jquery-ui/jquery-ui.js',
          'vendor/bower_components/fullcalendar/fullcalendar.js',
          'vendor/bower_components/angular-ui-calendar/src/calendar.js',
          'vendor/bower_components/angular-bootstrap-datetimepicker/src/js/datetimepicker.js',
          'vendor/bower_components/slider/dist/slider.js',
          'vendor/bower_components/oclazyload/dist/ocLazyLoad.js',
          'vendor/iscroll-lite.js',
          'vendor/orangebox.js',
          'vendor/mobiscroll.custom-2.16.0.min.js',
          'assets/javascripts/app.module.js',
          'services/models.js',
          'services/factories.js',
          'directives/directives.js',
          'controllers/SchedulingController.js',
          'controllers/CheckinController.js',
          'controllers/DashboardController.js',
          'controllers/ReportsController.js',
          'controllers/SettingsController.js',
          'controllers/StaffController.js',
          'controllers/VehicleController.js',
          'controllers/LoginController.js',
          'controllers/HeaderController.js',
          'controllers/CheckinModalController.js',
          'assets/javascripts/main.js',
        ]
      }
    }
  },

  cssmin: {
    options: {
      shorthandCompacting: false,
      roundingPrecision: -1
    },
    target: {
      files: {
        'assets/stylesheets/all.min.css': [
          'vendor/opensans-fonts.css',
          "vendor/calendar.css",
          "vendor/jquery-ui.css",
          "vendor/jquery-ui.structure.css",
          "vendor/jquery-ui.theme.css",
          "assets/stylesheets/layout.css",
          "assets/stylesheets/light.css",
          "assets/stylesheets/login.css",
          "assets/stylesheets/plugins.css",

          "vendor/bower_components/bootstrap/bootstrap.css",
          "vendor/jquery.datetimepicker.css",
          "vendor/bootstrap-switch.css",
          "vendor/bower_components/angular-loading-bar/src/loading-bar.css",
          "vendor/font-awesome.css",
          "vendor/bower_components/fullcalendar/fullcalendar.css",
          "vendor/bower_components/angular-tablesort/tablesort.css",
          "vendor/bower_components/angular-xeditable/dist/css/xeditable.css",
          "vendor/mobiscroll.custom-2.16.0.min.css",
          "vendor/dropzone.css",
          "vendor/orangebox.css",
          "vendor/google-fonts.css",
          "assets/stylesheets/components.css",
          "assets/stylesheets/main.css",
        ]
      }
    }
  },

  cachebreaker: {
      dev: {
        options: {
          match: ['all-login.*.js', 'all.*.js', 'all.*.css'],
        },
        files: {
          src: ['index.html', 'login.html']
        }
      }
  },

  concurrent: {
    css_and_js: ['uglify', 'cssmin'],
  }
});

// TASKS
grunt.registerTask('default', ['concurrent:css_and_js', 'cachebreaker']);

};

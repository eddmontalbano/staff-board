DigiboardApp.factory('User', function(DS, $http, $q, $cookieStore) {
  var self;
  var ct=0;
  var User = DS.defineResource({
    name: 'users',
    maxAge: null,
    methods: {
      canEdit: function(user) {
        //console.log(++ct);
        return false;
      }
    },
    relations: {
      belongsTo: {
        users: {
          localField: 'backup_supervisor',
          localKey: 'backup_supervisor_id'
        }
      },
      hasOne: {
        checkins: {
          localField: 'checkin',
          foreignKey: 'user_id'
        }
      }
    }
  });

  User.login = function(username, passcode, code) {
    var deferred = $q.defer();

    $http.post(window.api_url + '/login', {
      name: username,
      password: passcode,
      code: code

    }).success(function (data) {
      var cookie_name = 'digitalboard_user';
      var now = new Date();
      var exp = new Date(now.getFullYear(), now.getMonth() + 6, now.getDate());
      $cookieStore.put(cookie_name, data, {expires: exp});
      deferred.resolve(data);

    }).error(function (data) {
      deferred.reject(data);
    });

    return deferred.promise;
  }

  return User;
});

DigiboardApp.factory('Session', function(DS) {
   var Session = DS.defineResource({
     name: 'sessions',
     maxAge: null,
     relations: {
       belongsTo: {
         users: {
           localField: 'live',
           localKey: 'user_id'
         }
       }
     }
   });

   return Session;
});


DigiboardApp.factory('CheckinHistory', function(DS, $http){
  var Checkin = DS.defineResource({
    name: 'checkin_histories',
    //maxAge: 120000, // Refresh checkins every 2 minutes
    relations: {
      belongsTo: {
        users: {
          localField: 'user',
          localKey: 'user_id'
        },
        vehicles: {
          localField: 'vehicle',
          localKey: 'vehicle_id'
        },
        projects: [{
          localField: 'project',
          localKey: 'project_id'
        },
        {
          localField: 'phase',
          localKey: 'phase_id'
        },
        {
          localField: 'subphase',
          localKey: 'subphase_id'
        },
        {
          localField: 'subsub',
          localKey: 'subsub_id'
        }]
      }
    }
  });

  Checkin.generate_report = function(start_date, end_date) {
    $http.get(window.api_url + '/checkin_histories/report.csv?start_date=' + start_date
      + '&end_date=' + end_date).success(function(data, status, headers, config){
      console.log("successfully generated report")
      var element = angular.element('<a/>');
      element.attr({
         href: 'data:attachment/csv;charset=utf-8,' + encodeURI(data),
         target: '_blank',
         download: 'report_'+ ((new Date()).getTime()) + '.csv'
      })[0].click();
    }).error(function(response){
      console.log("error generating report")
    });
  }

  return Checkin;

});

DigiboardApp.factory('Checkin', function(DS, User, $rootScope){
  var Checkin = DS.defineResource({
    name: 'checkins',
    //maxAge: 120000, // Refresh checkins every 2 minutes
    // beforeReap: function(resource, data) {
    //   Checkin.findAll({}, {bypassCache:true}).then(function(data){
    //     console.log('checkin data:');
    //     // DS.findAll('projects', {}, {bypassCache:true}).then(function() {
    //     //   User.linkAll();
    //     //   for (var i=0; i<data.length; i++) {
    //     //     $rootScope.checkLinks(data[i], true);
    //     //   }
    //     // });
    //   });
    // },
    // reapAction: "inject",
    relations: {
      belongsTo: {
        users: {
          localField: 'user',
          localKey: 'user_id'
        },
        vehicles: {
          localField: 'vehicle',
          localKey: 'vehicle_id'
        },
        projects: [{
          localField: 'project',
          localKey: 'project_id'
        },
        {
          localField: 'phase',
          localKey: 'phase_id'
        },
        {
          localField: 'subphase',
          localKey: 'subphase_id'
        },
        {
          localField: 'subsub',
          localKey: 'subsub_id'
        }]
      }
    }
  });

  return Checkin;

});

DigiboardApp.factory('Dashboard', function(DS){
  return DS.defineResource({
    name: 'dashboard'
  });
});


DigiboardApp.factory('Schedule', function(DS, $rootScope){
  var Schedule = DS.defineResource({
    name: 'schedules',
    //maxAge: 120000, // Refresh checkins every 2 minutes
    relations: {
      belongsTo: {
        users: {
          localField: 'user',
          localKey: 'user_id'
        },
        vehicles: {
          localField: 'vehicle',
          localKey: 'vehicle_id'
        }
      }
    },
    computed: {
      title: ['user_id', 'vehicle_id', 'comment', 'start', 'end', function(user_id, vehicle_id, comment, start, end) {
        var title = "";
        try {
          if (vehicle_id) title += DS.get('vehicles', vehicle_id).name + " - ";
        } catch(e) {}
        try {
          if (user_id) title += DS.get('users', user_id).name + " - ";
        } catch(e) {}

        // var s = new Date(start),
        //   e = new Date(end),
        //   timestr = function(d) {
        //     var ts = d.toLocaleTimeString('en', {hour12: true}),
        //       offset = (ts[1] == ":") ? 0 : 1;
        //     return ts.substr(0,4+offset) + ts[8 + offset];
        //   }
        //
        //
        // return timestr(s) + "-" + timestr(e) + " | " + title.substr(0, title.length-3);
        return title.substr(0, title.length-3);
      }]
    },
    beforeValidate: function(resource, attrs, cb) {
      $rootScope.error = undefined;
      var tzo = new Date().getTimezoneOffset()

      if (!attrs.schedule || !attrs.schedule.start) {
        console.log(attrs);
        $rootScope.error = "No start time specificed";
        throw 'No start time specified';
      }

      if (!attrs.schedule.end) {
        attrs.schedule.end = moment(attrs.schedule.start).add('days', 1)
      }

      attrs.schedule.start = new Date(attrs.schedule.start);
      attrs.schedule.end = new Date(attrs.schedule.end);

      var now = new Date();

      // Run validation rules against scheduling
      if (attrs.schedule.end.getTime() < now.getTime()){
        $rootScope.error = 'Cannot schedule on past dates';
        throw  'Cannot schedule past dates';
      }

      if (attrs.schedule.end.getTime() < attrs.schedule.start.getTime()){
        $rootScope.error = 'Invalid scheduling: Return time ends before start time.';
        throw 'Invalid scheduling: Return time ends before start time.';

      }

      if (!attrs.schedule.vehicle && !attrs.schedule.vehicle_id) {
         // $rootScope.error = 'No vehicle specified';
        //throw 'No vehicle specified';

      }

      //Handle associating relationships
      if (attrs.schedule.user)
        attrs.schedule.user_id = attrs.schedule.user.id;
      if (attrs.schedule.vehicle)
        attrs.schedule.vehicle_id = attrs.schedule.vehicle.id;

      attrs.schedule.start.setMinutes(attrs.schedule.start.getMinutes()-tzo);
      attrs.schedule.end.setMinutes(attrs.schedule.end.getMinutes()-tzo);
      //Check for scheduling overlap
      var overlap = Schedule.filter({
        where: {
          vehicle_id: {'==': attrs.schedule.vehicle_id },
          end: {'>': attrs.schedule.start },
          start: {'<': attrs.schedule.end }
      }});

      if (overlap.length > 0 && !(overlap[0].id === attrs.schedule.id)) {
        attrs.schedule.start.setMinutes(attrs.schedule.start.getMinutes()+tzo);
        attrs.schedule.end.setMinutes(attrs.schedule.end.getMinutes()+tzo);
        $rootScope.error = 'Vehicle is already scheduled:';
        throw 'Vehicle is already scheduled';

      }

      cb(null, attrs);
    }
  });

  Schedule.scheduled = function(date, range) {
    if (date === undefined) {
      date = new Date();
      date.setHours(0);
    }
    if (range === undefined) {
      range = new Date(date.toString());
      range.setHours(23);
    }

    var evts = Schedule.filter({
      where: {
        end: {'>': date },
        start: {'<': range }
      }});
    return evts;
  }

  return Schedule;

});


DigiboardApp.factory('Vehicle', function(DS) {
  var Vehicle = DS.defineResource({
    name: 'vehicles',
    maxAge: null,
    methods: {
      isOut: function() {
        return (this.checkin !== undefined)
      }
    },
    relations: {
      hasOne: {
        checkins: {
          localField: 'checkin',
          foreignKey: 'vehicle_id'
        }
      },
      belongsTo: {
        users: {
          localField: 'user',
          localKey: 'user_id'
        }
      }
    }

  });
  return Vehicle;
});

DigiboardApp.factory('Project', function(DS) {
  var Project = DS.defineResource({
    name: 'projects',
    maxAge: null,
    relations: {
      hasMany: { // Attaches subprojects to each project
        projects: {
          localField: 'projects',
          foreignKey: 'project_id'
        }
      },
      belongsTo: {
        projects: {
          localKey: 'project_id',
          localField: 'parent',
          parent:true
        }
      }
    }

  });
  return Project;
});

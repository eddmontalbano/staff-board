// ------------------------------------------------------------ //
// -------------------- CONSTANTS / HELPERS ------------------- //
// ------------------------------------------------------------ //

'use strict';

//
// Dynamic api path, based on production/development environment:
//
window.api_url = 'blanked';
if ((window.location.protocol == 'file:') || (window.location.hostname == 'localhost')) {
  window.api_url = 'http://localhost:3000/';
}   
else if (window.location.hostname == 'blanked') {
  window.api_url = 'blanked';
}

//
// Orange Box plugin config
//
oB.settings.addThis = false;

//
// Extends JS's "Array" to add an "unique" method
//
// PS: this is used to prevent duplicates while generating "editUsersMobile"
//
Array.prototype.unique = function() {
  var unique = [];
  for (var i = 0; i < this.length; i++) {
    if (unique.indexOf(this[i]) == -1) {
      unique.push(this[i]);
    }
  }
  return unique;
};
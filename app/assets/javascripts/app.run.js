DigiboardApp.run([ '$rootScope', 'settings', '$state', 'editableOptions', 'editableThemes', '$cookieStore',
  function ( $rootScope, settings, $state, editableOptions, editableThemes, $cookieStore ) {

    // Clear checkin list
    $rootScope.staff = [];
    $rootScope.editUsers = [];
    $rootScope.editUsersMobile = [];

    $rootScope.$state = $state;
    $rootScope.api_url = window.api_url;

    // x-editable
    editableThemes.bs3.inputClass = 'form-control';
    editableThemes.bs3.buttonsClass = 'btn-icon-only';
    editableOptions.theme = 'bs3';

    //
    // Session cookie
    //
    $rootScope.cookieName = 'digitalboard_user';
    $rootScope.cookieContents = $cookieStore.get($rootScope.cookieName);

    //
    // Window size: Desktop or Mobile
    //
    $rootScope.is_desktop = window.innerWidth >= 992;
    $rootScope.is_large = window.innerWidth >= 1200;

  }]);
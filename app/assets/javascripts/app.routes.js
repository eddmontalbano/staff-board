
DigiboardApp.config(['DSProvider', '$stateProvider', '$urlRouterProvider', '$locationProvider', '$httpProvider',
  function ( DSProvider, $stateProvider, $urlRouterProvider, $locationProvider, $httpProvider ) {

    DSProvider.defaults.basePath = window.api_url;

    $httpProvider.defaults.withCredentials = true;

    if (window.history && window.history.pushState) {
      $locationProvider.html5Mode(true);
    }

    $urlRouterProvider.otherwise("/");

    $stateProvider
      .state('checkin', {
        url: "/",
        templateUrl: "views/checkin.html",
        data: {pageTitle: 'Check-in Board'},
        controller: "CheckinController",
        resolve: {
          deps: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: 'DigiboardApp',
              insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: [
                '/controllers/CheckinController.js'
              ]
            });
          }]
        }
      })

      .state('settings', {
        url: "/settings",
        templateUrl: "views/settings.html",
        data: {pageTitle: 'App Settings'},
        controller: "SettingsController",
        resolve: {
          deps: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: 'DigiboardApp',
              insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: [
                '/controllers/SettingsController.js'
              ]
            });
          }]
        }
      })

      .state('schedule', {
        url: "/scheduling",
        templateUrl: "views/scheduling.html",
        data: {pageTitle: 'Scheduling Calendar'},
        controller: "DashboardController",
        resolve: {
          deps: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: 'DigiboardApp',
              insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: [
                '/controllers/DashboardController.js'
              ]
            });
          }]
        }
      })

      .state('staff', {
        url: "/staff",
        templateUrl: "views/staff.html",
        data: {pageTitle: 'Manage Staff'},
        controller: "StaffController",
        resolve: {
          deps: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: 'DigiboardApp',
              insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: [
                '/controllers/StaffController.js'
              ]
            });
          }]
        }
      })

      .state('projects', {
        url: "/project_manager",
        templateUrl: "views/projects.html",
        data: {pageTitle: 'Manage Projects'},
        controller: "DashboardController",
        resolve: {
          deps: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: 'DigiboardApp',
              insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: [
                '/controllers/DashboardController.js'
              ]
            });
          }]
        }
      })

      .state('reports', {
        url: "/reports",
        templateUrl: "views/reports.html",
        data: {pageTitle: 'Reports'},
        controller: "ReportsController",
        resolve: {
          deps: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: 'DigiboardApp',
              insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: [
                '/controllers/ReportsController.js'
              ]
            });
          }]
        }
      })

      .state('transportation', {
        url: "/transportation",
        templateUrl: "views/transportation.html",
        data: {pageTitle: 'Manage Vehicle Inventory'},
        controller: "VehicleController",
        resolve: {
          deps: ['$ocLazyLoad', function($ocLazyLoad) {
            return $ocLazyLoad.load({
              name: 'DigiboardApp',
              insertBefore: '#ng_load_plugins_before', // load the above css files before a LINK element with this ID. Dynamic CSS files must be loaded between core and theme css files
              files: [
                '/controllers/VehicleController.js'
              ]
            });
          }]
        }
      });
  }]);
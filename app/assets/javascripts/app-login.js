'use strict';

// dynamic api path, based on production/development environment
window.api_url = 'blanked';
if ((window.location.protocol == 'file:') || (window.location.hostname == 'localhost')) {
  window.api_url = 'http://localhost:3000/';
}
else if (window.location.hostname == 'blanked') {
  window.api_url = 'blanked';
}

var LoginApp = angular.module('LoginApp', [
  'ui.router',
  'ngCookies'
]);

LoginApp.controller('LoginController', function ($scope, $rootScope, $http, $cookieStore) {
  $scope.login = {};
  $scope.error = '';
  $scope.isProcessing = false; // prevents multiple button press

  $rootScope.doLogin = function() {
    if ($scope.isProcessing) return ;

    $scope.isProcessing = true;

    $http.post(window.api_url + '/login', {
      name: $scope.login.username,
      password: $scope.login.pass
    }).success(function (data) {
      var cookie_name = 'digitalboard_user';
      var now = new Date();
      var exp = new Date(now.getFullYear(), now.getMonth() + 6, now.getDate());
      $cookieStore.put(cookie_name, data, {expires: exp});
      window.location.href = '/'; // success, redirects to '/'

    }).error(function (data) {
      $scope.isProcessing = false;
      $scope.error = data.error;
    });
  }
});

LoginApp.config([
  '$stateProvider',
  '$urlRouterProvider',
  '$locationProvider',

  function (
    $stateProvider,
    $urlRouterProvider,
    $locationProvider
  ) {

  if (window.history && window.history.pushState) {
    $locationProvider.html5Mode(true);
  }

  $urlRouterProvider.otherwise('/login');

  $stateProvider.state('/login', {
    url: '/login',
    templateUrl: 'views/login.html',
    data: { pageTitle: 'Login' },
    controller: 'LoginController'
  })
}]);

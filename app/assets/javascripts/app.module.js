
'use strict';


var DigiboardApp = angular.module('DigiboardApp', [
  'ui.router',
  'ui.bootstrap',
  'oc.lazyLoad',
  'ngSanitize',
  'angular-loading-bar',
  'js-data',
  'xeditable',
  'mobiscroll-datetime',
  'mobiscroll-select',
  'ui.slider',
  'ui.bootstrap.datetimepicker',
  'ui.calendar',
  'frapontillo.bootstrap-switch',
  'ngCookies',
]);



--
-- PostgreSQL database dump
--

-- Dumped from database version 9.4.1
-- Dumped by pg_dump version 9.4.0
-- Started on 2015-03-09 10:08:23

SET statement_timeout = 0;
SET lock_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SET check_function_bodies = false;
SET client_min_messages = warning;

SET search_path = public, pg_catalog;

SET default_tablespace = '';

SET default_with_oids = false;

--
-- TOC entry 181 (class 1259 OID 73780)
-- Name: projects; Type: TABLE; Schema: public; Owner: elton; Tablespace: 
--

CREATE TABLE projects (
    id integer NOT NULL,
    name character varying,
    project_id integer,
    erp_path character varying,
    erp_name character varying,
    ax_id integer
);


ALTER TABLE projects OWNER TO elton;

--
-- TOC entry 180 (class 1259 OID 73778)
-- Name: projects_id_seq; Type: SEQUENCE; Schema: public; Owner: elton
--

CREATE SEQUENCE projects_id_seq
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;


ALTER TABLE projects_id_seq OWNER TO elton;

--
-- TOC entry 2051 (class 0 OID 0)
-- Dependencies: 180
-- Name: projects_id_seq; Type: SEQUENCE OWNED BY; Schema: public; Owner: elton
--

ALTER SEQUENCE projects_id_seq OWNED BY projects.id;


--
-- TOC entry 1932 (class 2604 OID 73783)
-- Name: id; Type: DEFAULT; Schema: public; Owner: elton
--

ALTER TABLE ONLY projects ALTER COLUMN id SET DEFAULT nextval('projects_id_seq'::regclass);


--
-- TOC entry 2046 (class 0 OID 73780)
-- Dependencies: 181
-- Data for Name: projects; Type: TABLE DATA; Schema: public; Owner: elton
--

INSERT INTO projects VALUES (1, 'Lightrail Expansion - Irvington', NULL, NULL, NULL, 1);
INSERT INTO projects VALUES (38, 'Admin', 34, 'Y2ZhNjk0MzVhMjczZTYxN2I2ZWNmZTE3ZjI0OTJiOGY=', 'Interface Mockup (1).pdf', 42);
INSERT INTO projects VALUES (55, 'Bottom-up', NULL, 'Y2ZhNjk0MzVhMjczZTYxN2I2ZWNmZTE3ZjI0OTJiOGY=', 'Interface Mockup (1).pdf', 59);
INSERT INTO projects VALUES (7, 'Rocky Point Wastewater System', NULL, NULL, NULL, 7);
INSERT INTO projects VALUES (28, 'Brentwood Estates', NULL, NULL, NULL, 28);
INSERT INTO projects VALUES (30, 'Portland Parcel Mapping', NULL, NULL, NULL, 30);
INSERT INTO projects VALUES (32, 'Irving Lofts Parking Lot', NULL, NULL, NULL, 32);
INSERT INTO projects VALUES (34, 'Z-Overhead', NULL, NULL, NULL, 34);
INSERT INTO projects VALUES (43, 'Top-down', NULL, NULL, NULL, 47);
INSERT INTO projects VALUES (101, 'Quick Project', NULL, NULL, NULL, 105);
INSERT INTO projects VALUES (105, 'Haven Street Garage', NULL, NULL, NULL, 155);
INSERT INTO projects VALUES (107, 'Bottom-up with Business Development', NULL, NULL, NULL, 158);
INSERT INTO projects VALUES (3, 'Schematic Design', 1, NULL, NULL, 3);
INSERT INTO projects VALUES (5, 'Final Review', 1, NULL, NULL, 5);
INSERT INTO projects VALUES (6, 'Scope Expansion', 1, NULL, NULL, 6);
INSERT INTO projects VALUES (8, 'Predesign', 7, NULL, NULL, 8);
INSERT INTO projects VALUES (9, 'Contract Drawing', 7, NULL, NULL, 9);
INSERT INTO projects VALUES (10, 'Project Manual', 7, NULL, NULL, 10);
INSERT INTO projects VALUES (11, 'Bidding & Negotiation', 7, NULL, NULL, 11);
INSERT INTO projects VALUES (12, 'Construction Administration', 7, NULL, NULL, 12);
INSERT INTO projects VALUES (14, 'Schematic Design', 13, NULL, NULL, 14);
INSERT INTO projects VALUES (15, 'Design Development', 13, NULL, NULL, 15);
INSERT INTO projects VALUES (17, 'Bidding & Negotiation', 13, NULL, NULL, 17);
INSERT INTO projects VALUES (18, 'Construction Administration', 13, NULL, NULL, 18);
INSERT INTO projects VALUES (20, 'Feasibility', 40, NULL, NULL, 20);
INSERT INTO projects VALUES (21, 'Conceptual Engineering', 40, NULL, NULL, 21);
INSERT INTO projects VALUES (22, 'Site/Lot Design', 40, NULL, NULL, 22);
INSERT INTO projects VALUES (23, 'General', 41, NULL, NULL, 23);
INSERT INTO projects VALUES (25, 'Plat Design', 41, NULL, NULL, 25);
INSERT INTO projects VALUES (26, 'Boundary Survey', 42, NULL, NULL, 26);
INSERT INTO projects VALUES (27, 'Topo Survey', 42, NULL, NULL, 27);
INSERT INTO projects VALUES (29, 'Land Survey', 28, NULL, NULL, 29);
INSERT INTO projects VALUES (31, 'Sellwood Parcel Mapping', 30, NULL, NULL, 31);
INSERT INTO projects VALUES (33, 'Survey', 32, NULL, NULL, 33);
INSERT INTO projects VALUES (35, 'Civil', 34, NULL, NULL, 39);
INSERT INTO projects VALUES (39, 'Bidding & Negotiation', 1, NULL, NULL, 43);
INSERT INTO projects VALUES (40, 'Planning', 103, NULL, NULL, 44);
INSERT INTO projects VALUES (41, 'Engineering', 103, NULL, NULL, 45);
INSERT INTO projects VALUES (42, 'Survey', 103, NULL, NULL, 46);
INSERT INTO projects VALUES (44, 'Planning', 43, NULL, NULL, 48);
INSERT INTO projects VALUES (45, 'Feasibility', 44, NULL, NULL, 49);
INSERT INTO projects VALUES (47, 'Site/Lot Design', 44, NULL, NULL, 51);
INSERT INTO projects VALUES (48, 'Engineering', 43, NULL, NULL, 52);
INSERT INTO projects VALUES (49, 'General', 48, NULL, NULL, 53);
INSERT INTO projects VALUES (50, 'Environmental Design', 48, NULL, NULL, 54);
INSERT INTO projects VALUES (51, 'Plat Design', 48, NULL, NULL, 55);
INSERT INTO projects VALUES (52, 'Survey', 43, NULL, NULL, 56);
INSERT INTO projects VALUES (54, 'Topo Survey', 52, NULL, NULL, 58);
INSERT INTO projects VALUES (56, 'Planning', 55, NULL, NULL, 60);
INSERT INTO projects VALUES (57, 'Feasibility', 56, NULL, NULL, 61);
INSERT INTO projects VALUES (58, 'Conceptual Engineering', 56, NULL, NULL, 62);
INSERT INTO projects VALUES (59, 'Site/Lot Design', 56, NULL, NULL, 63);
INSERT INTO projects VALUES (60, 'Engineering', 55, NULL, NULL, 64);
INSERT INTO projects VALUES (61, 'General', 60, NULL, NULL, 65);
INSERT INTO projects VALUES (63, 'Plat Design', 60, NULL, NULL, 67);
INSERT INTO projects VALUES (64, 'Survey', 55, NULL, NULL, 68);
INSERT INTO projects VALUES (65, 'Boundary Survey', 64, NULL, NULL, 69);
INSERT INTO projects VALUES (66, 'Topo Survey', 64, NULL, NULL, 70);
INSERT INTO projects VALUES (67, 'Research', 57, NULL, NULL, 71);
INSERT INTO projects VALUES (68, 'Engineering', 57, NULL, NULL, 72);
INSERT INTO projects VALUES (69, 'Planning', 57, NULL, NULL, 73);
INSERT INTO projects VALUES (70, 'Drafting', 57, NULL, NULL, 74);
INSERT INTO projects VALUES (72, 'Consultant Coordination', 57, NULL, NULL, 76);
INSERT INTO projects VALUES (73, 'Meetings', 57, NULL, NULL, 77);
INSERT INTO projects VALUES (74, 'Research', 58, NULL, NULL, 78);
INSERT INTO projects VALUES (75, 'Designs', 58, NULL, NULL, 79);
INSERT INTO projects VALUES (76, 'Drafts', 58, NULL, NULL, 80);
INSERT INTO projects VALUES (77, 'Consultant Coordination', 58, NULL, NULL, 81);
INSERT INTO projects VALUES (78, 'Meetings', 58, NULL, NULL, 82);
INSERT INTO projects VALUES (80, 'Plans & Exhibit Maps', 59, NULL, NULL, 84);
INSERT INTO projects VALUES (81, 'Meetings', 59, NULL, NULL, 85);
INSERT INTO projects VALUES (82, 'Research', 61, NULL, NULL, 86);
INSERT INTO projects VALUES (83, 'Designs', 61, NULL, NULL, 87);
INSERT INTO projects VALUES (84, 'Drafts', 61, NULL, NULL, 88);
INSERT INTO projects VALUES (85, 'Drafting', 61, NULL, NULL, 89);
INSERT INTO projects VALUES (87, 'Meetings', 61, NULL, NULL, 91);
INSERT INTO projects VALUES (88, 'Research', 62, NULL, NULL, 92);
INSERT INTO projects VALUES (89, 'Cost Estimate', 62, NULL, NULL, 93);
INSERT INTO projects VALUES (90, 'Grading', 62, NULL, NULL, 94);
INSERT INTO projects VALUES (91, 'Drafting', 62, NULL, NULL, 95);
INSERT INTO projects VALUES (92, 'Streets', 62, NULL, NULL, 96);
INSERT INTO projects VALUES (93, 'Water', 62, NULL, NULL, 97);
INSERT INTO projects VALUES (94, 'Sanitary', 62, NULL, NULL, 98);
INSERT INTO projects VALUES (95, 'Pumps', 62, NULL, NULL, 99);
INSERT INTO projects VALUES (97, 'Drafting', 63, NULL, NULL, 101);
INSERT INTO projects VALUES (98, 'Plat Checks', 63, NULL, NULL, 102);
INSERT INTO projects VALUES (99, 'Review', 63, NULL, NULL, 103);
INSERT INTO projects VALUES (100, 'Meetings', 63, NULL, NULL, 104);
INSERT INTO projects VALUES (102, 'Lot Survey', 101, NULL, NULL, 106);
INSERT INTO projects VALUES (104, 'Additional Services', 19, NULL, NULL, 154);
INSERT INTO projects VALUES (106, 'Lot Survey', 105, NULL, NULL, 157);
INSERT INTO projects VALUES (108, 'Planning', 107, NULL, NULL, 159);
INSERT INTO projects VALUES (109, 'Feasibility', 108, NULL, NULL, 160);
INSERT INTO projects VALUES (110, 'Research', 109, NULL, NULL, 161);
INSERT INTO projects VALUES (111, 'Engineering', 109, NULL, NULL, 162);
INSERT INTO projects VALUES (112, 'Planning', 109, NULL, NULL, 163);
INSERT INTO projects VALUES (113, 'Drafting', 109, NULL, NULL, 164);
INSERT INTO projects VALUES (114, 'Construction Cost Estimate', 109, NULL, NULL, 165);
INSERT INTO projects VALUES (115, 'Consultant Coordination', 109, NULL, NULL, 166);
INSERT INTO projects VALUES (116, 'Meetings', 109, NULL, NULL, 167);
INSERT INTO projects VALUES (118, 'Research', 117, NULL, NULL, 169);
INSERT INTO projects VALUES (119, 'Designs', 117, NULL, NULL, 170);
INSERT INTO projects VALUES (120, 'Drafts', 117, NULL, NULL, 171);
INSERT INTO projects VALUES (121, 'Consultant Coordination', 117, NULL, NULL, 172);
INSERT INTO projects VALUES (122, 'Meetings', 117, NULL, NULL, 173);
INSERT INTO projects VALUES (123, 'Site/Lot Design', 108, NULL, NULL, 174);
INSERT INTO projects VALUES (125, 'Plans & Exhibit Maps', 123, NULL, NULL, 176);
INSERT INTO projects VALUES (126, 'Meetings', 123, NULL, NULL, 177);
INSERT INTO projects VALUES (127, 'Engineering', 107, NULL, NULL, 178);
INSERT INTO projects VALUES (128, 'General', 127, NULL, NULL, 179);
INSERT INTO projects VALUES (129, 'Research', 128, NULL, NULL, 180);
INSERT INTO projects VALUES (130, 'Designs', 128, NULL, NULL, 181);
INSERT INTO projects VALUES (131, 'Drafts', 128, NULL, NULL, 182);
INSERT INTO projects VALUES (133, 'Consultant Coordination', 128, NULL, NULL, 184);
INSERT INTO projects VALUES (134, 'Meetings', 128, NULL, NULL, 185);
INSERT INTO projects VALUES (135, 'Environmental Design', 127, NULL, NULL, 186);
INSERT INTO projects VALUES (136, 'Research', 135, NULL, NULL, 187);
INSERT INTO projects VALUES (137, 'Cost Estimate', 135, NULL, NULL, 188);
INSERT INTO projects VALUES (138, 'Grading', 135, NULL, NULL, 189);
INSERT INTO projects VALUES (140, 'Streets', 135, NULL, NULL, 191);
INSERT INTO projects VALUES (141, 'Water', 135, NULL, NULL, 192);
INSERT INTO projects VALUES (142, 'Sanitary', 135, NULL, NULL, 193);
INSERT INTO projects VALUES (143, 'Pumps', 135, NULL, NULL, 194);
INSERT INTO projects VALUES (145, 'Setup', 144, NULL, NULL, 196);
INSERT INTO projects VALUES (146, 'Drafting', 144, NULL, NULL, 197);
INSERT INTO projects VALUES (147, 'Plat Checks', 144, NULL, NULL, 198);
INSERT INTO projects VALUES (148, 'Review', 144, NULL, NULL, 199);
INSERT INTO projects VALUES (149, 'Meetings', 144, NULL, NULL, 200);
INSERT INTO projects VALUES (150, 'Survey', 107, NULL, NULL, 201);
INSERT INTO projects VALUES (152, 'Topo Survey', 150, NULL, NULL, 203);
INSERT INTO projects VALUES (153, 'Business Development', 107, NULL, NULL, 204);
INSERT INTO projects VALUES (154, 'Top-down with Business Development', NULL, NULL, NULL, 205);
INSERT INTO projects VALUES (13, 'Wilson Elementary School', NULL, 'NjAyZWMxMDFlZDk0OWNjYTc4ZGMwMzVkZGE0OWRjMTk=', 'Truck Board Interface (1).pdf', 13);
INSERT INTO projects VALUES (37, 'Structural', 34, 'Y2ZhNjk0MzVhMjczZTYxN2I2ZWNmZTE3ZjI0OTJiOGY=', 'Interface Mockup (2).pdf', 41);
INSERT INTO projects VALUES (36, 'Environmental', 34, 'Y2ZhNjk0MzVhMjczZTYxN2I2ZWNmZTE3ZjI0OTJiOGY=', 'Interface Mockup (1).pdf', 40);
INSERT INTO projects VALUES (19, 'Highland Shopping Center', NULL, 'Y2ZhNjk0MzVhMjczZTYxN2I2ZWNmZTE3ZjI0OTJiOGY=', 'Interface Mockup (1).pdf', 19);
INSERT INTO projects VALUES (170, 'Cooke County Parking Structure', NULL, NULL, NULL, 221);
INSERT INTO projects VALUES (172, 'Virginia Avenue Business Park', NULL, NULL, NULL, 223);
INSERT INTO projects VALUES (185, 'Combination Template', NULL, NULL, NULL, 236);
INSERT INTO projects VALUES (2, 'Predesign', 1, NULL, NULL, 2);
INSERT INTO projects VALUES (4, 'Construction Documentation', 1, NULL, NULL, 4);
INSERT INTO projects VALUES (16, 'Construction Documents', 13, NULL, NULL, 16);
INSERT INTO projects VALUES (24, 'Environmental Design', 41, NULL, NULL, 24);
INSERT INTO projects VALUES (46, 'Conceptual Engineering', 44, NULL, NULL, 50);
INSERT INTO projects VALUES (53, 'Boundary Survey', 52, NULL, NULL, 57);
INSERT INTO projects VALUES (62, 'Environmental Design', 60, NULL, NULL, 66);
INSERT INTO projects VALUES (71, 'Construction Cost Estimate', 57, NULL, NULL, 75);
INSERT INTO projects VALUES (79, 'Site / Lot Design', 59, NULL, NULL, 83);
INSERT INTO projects VALUES (86, 'Consultant Coordination', 61, NULL, NULL, 90);
INSERT INTO projects VALUES (96, 'Setup', 63, NULL, NULL, 100);
INSERT INTO projects VALUES (103, 'Basic Services', 19, NULL, NULL, 153);
INSERT INTO projects VALUES (117, 'Conceptual Engineering', 108, NULL, NULL, 168);
INSERT INTO projects VALUES (124, 'Site / Lot Design', 123, NULL, NULL, 175);
INSERT INTO projects VALUES (132, 'Drafting', 128, NULL, NULL, 183);
INSERT INTO projects VALUES (139, 'Drafting', 135, NULL, NULL, 190);
INSERT INTO projects VALUES (144, 'Plat Design', 127, NULL, NULL, 195);
INSERT INTO projects VALUES (151, 'Boundary Survey', 150, NULL, NULL, 202);
INSERT INTO projects VALUES (155, 'Planning', 154, NULL, NULL, 206);
INSERT INTO projects VALUES (156, 'Feasibility', 155, NULL, NULL, 207);
INSERT INTO projects VALUES (157, 'Conceptual Engineering', 155, NULL, NULL, 208);
INSERT INTO projects VALUES (158, 'Site/Lot Design', 155, NULL, NULL, 209);
INSERT INTO projects VALUES (159, 'Engineering', 154, NULL, NULL, 210);
INSERT INTO projects VALUES (160, 'General', 159, NULL, NULL, 211);
INSERT INTO projects VALUES (161, 'Environmental Design', 159, NULL, NULL, 212);
INSERT INTO projects VALUES (162, 'Plat Design', 159, NULL, NULL, 213);
INSERT INTO projects VALUES (163, 'Survey', 154, NULL, NULL, 214);
INSERT INTO projects VALUES (164, 'Boundary Survey', 163, NULL, NULL, 215);
INSERT INTO projects VALUES (165, 'Topo Survey', 163, NULL, NULL, 216);
INSERT INTO projects VALUES (166, 'Business Development', 154, NULL, NULL, 217);
INSERT INTO projects VALUES (167, 'Business Development', 7, NULL, NULL, 218);
INSERT INTO projects VALUES (168, 'Business Development', 19, NULL, NULL, 219);
INSERT INTO projects VALUES (169, 'Add''l Services Business Developoment', 19, NULL, NULL, 220);
INSERT INTO projects VALUES (171, 'Business Development', 170, NULL, NULL, 222);
INSERT INTO projects VALUES (173, 'Planning', 172, NULL, NULL, 224);
INSERT INTO projects VALUES (174, 'Feasibility', 173, NULL, NULL, 225);
INSERT INTO projects VALUES (175, 'Conceptual Engineering', 173, NULL, NULL, 226);
INSERT INTO projects VALUES (176, 'Site/Lot Design', 173, NULL, NULL, 227);
INSERT INTO projects VALUES (177, 'Engineering', 172, NULL, NULL, 228);
INSERT INTO projects VALUES (178, 'General', 177, NULL, NULL, 229);
INSERT INTO projects VALUES (179, 'Environmental Design', 177, NULL, NULL, 230);
INSERT INTO projects VALUES (180, 'Plat Design', 177, NULL, NULL, 231);
INSERT INTO projects VALUES (181, 'Survey', 172, NULL, NULL, 232);
INSERT INTO projects VALUES (182, 'Boundary Survey', 181, NULL, NULL, 233);
INSERT INTO projects VALUES (183, 'Topo Survey', 181, NULL, NULL, 234);
INSERT INTO projects VALUES (184, 'Business Development', 172, NULL, NULL, 235);
INSERT INTO projects VALUES (186, 'Site Assessment', 185, NULL, NULL, 237);
INSERT INTO projects VALUES (187, 'Site Sampling', 185, NULL, NULL, 238);
INSERT INTO projects VALUES (188, 'Additional Services', 185, NULL, NULL, 239);
INSERT INTO projects VALUES (189, 'Main Contact', 170, NULL, NULL, 240);
INSERT INTO projects VALUES (190, 'Promotional Discount', 7, NULL, NULL, 241);


--
-- TOC entry 2052 (class 0 OID 0)
-- Dependencies: 180
-- Name: projects_id_seq; Type: SEQUENCE SET; Schema: public; Owner: elton
--

SELECT pg_catalog.setval('projects_id_seq', 190, true);


--
-- TOC entry 1935 (class 2606 OID 73788)
-- Name: projects_pkey; Type: CONSTRAINT; Schema: public; Owner: elton; Tablespace: 
--

ALTER TABLE ONLY projects
    ADD CONSTRAINT projects_pkey PRIMARY KEY (id);


--
-- TOC entry 1933 (class 1259 OID 73789)
-- Name: index_projects_on_project_id; Type: INDEX; Schema: public; Owner: elton; Tablespace: 
--

CREATE INDEX index_projects_on_project_id ON projects USING btree (project_id);


-- Completed on 2015-03-09 10:08:23

--
-- PostgreSQL database dump complete
--


namespace :crontasks do

  desc "Update database from staffDB"
  task update_staffDB: :environment do
    begin
      # log
      File.open("log/crontasks.log", "a+") {|f| f << "#{DateTime.now().strftime("%Y-%m-%d %H:%M:%S")}: UPDATE_staffDB START\n" }

      # extra libraries
      require 'rubygems'
      require 'tiny_tds'

      # record when this happened
      Setting.setKey('staffDB_last_run', DateTime.now())

      # summary
      users_count = 0
      projects_count = 0
      trucks_count = 0

      # trasaction: we don't want to save things by halves, right?
      ActiveRecord::Base.transaction do

        #
        # remote connection with Microsoft's SQL Server (staffDB)
        #
        client = staffDB_connect

        #
        # sync our staff database first
        #
        users_count = staff_sync(client)

        #
        # changed projects (to adjust project_id and parent_status)
        #
        changed_ax_ids = []

        #
        # sync project status: we check if the current projects still alive on staffDB (batches of 1,000)
        #
        ax_ids = Project.where(status: 1).pluck(:ax_id)
        while ax_ids.count != 0
          batch = ax_ids.slice!(0, 1000)

          # cache: projects[id] == Project.find_by(ax_id: id)
          projects = {}
          Project.where(ax_id: batch).each { |r| projects[r.ax_id] = r }

          staffDB_projects = client.execute("
            SELECT prjKey, prjDescription, prjParent, prjStatus
            FROM [dbo].[AxProject]
            WHERE
              prjStatus != 1 AND
              (prjKey IN (#{batch.join(',')}))"
          )
          staffDB_projects.each do |staffDB_project|
            project = projects[staffDB_project['prjKey']]

            # update data
            project.update_attributes({
              ax_id: staffDB_project['prjKey'],
              ax_parent: staffDB_project['prjParent'],
              name: staffDB_project['prjDescription'],
              status: staffDB_project['prjStatus']
            })

            # affected projects
            changed_ax_ids << project.ax_id

            # children status
            Project.
              where(project_id: project.id).
              update_all({parent_status: staffDB_project['prjStatus']})
          end
        end

        #
        # now, we sync the projects (batches of 1,000)
        #
        staffDB_projects_count = client.execute("
          SELECT COUNT(*) as count
          FROM [dbo].[AxProject]
          WHERE prjStatus = 1"
        ).first['count']

        # we sync it in batches
        batch_size = 1000
        loops = Integer(staffDB_projects_count / batch_size)
        (0..loops).each do |i|
          staffDB_projects_partial = client.execute("
            SELECT prjKey, prjDescription, prjParent, prjStatus
            FROM [dbo].[AxProject]
            WHERE prjStatus = 1
            ORDER BY prjKey
            OFFSET #{i * batch_size} ROWS FETCH NEXT #{batch_size} ROWS ONLY"
          )

          # cache: projects[id] == Project.find_by(ax_id: id)
          ax_ids = staffDB_projects_partial.map { |r| r['prjKey'] }
          projects = {}
          Project.where(ax_id: ax_ids).each { |r| projects[r.ax_id] = r }

          # for each staffDB record, update locally
          staffDB_projects_partial.each do |staffDB_project|
            project = projects[staffDB_project['prjKey']] || Project.new()
            projects_count += 1 if project.id.nil? # summary

            if project.id.nil? ||
              (project.ax_parent != staffDB_project['prjParent']) ||
              (project.status != staffDB_project['prjStatus']) ||
              (project.name != staffDB_project['prjDescription'])

              # affected projects
              changed_ax_ids << staffDB_project['prjKey']
              changed_ax_ids << staffDB_project['prjParent'] if staffDB_project['prjParent'].present?

              # update
              parent = staffDB_project['prjParent'].present? ? Project.find_by(ax_id: staffDB_project['prjParent']) : nil
              project.update_attributes({
                ax_id: staffDB_project['prjKey'],
                ax_parent: staffDB_project['prjParent'],
                name: staffDB_project['prjDescription'],
                status: staffDB_project['prjStatus'],
                parent_status: parent.try(:status) || true,
                project_id: parent.try(:id)
              })
            end
          end
        end

        #
        # adjust parenting (Project.project_id, based on Project.ax_parent)
        #
        Project.where(ax_id: changed_ax_ids.uniq).each do |parent|
          Project.
            where(ax_parent: parent.ax_id).
            update_all({
              parent_status: parent.status,
              project_id: parent.id
            })
        end

      end # commit transaction

      # log
      File.open("log/crontasks.log", "a+") {|f| f << "#{DateTime.now().strftime("%Y-%m-%d %H:%M:%S")}: UPDATE_staffDB SUCCESS - #{{:users => users_count, :projects => projects_count, :trucks => trucks_count}.to_json}\n" }

      puts ({:users => users_count, :projects => projects_count, :trucks => trucks_count}.to_json)

    rescue Exception => e
      # log
      File.open("log/crontasks.log", "a+") {|f| f << "#{DateTime.now().strftime("%Y-%m-%d %H:%M:%S")}: UPDATE_staffDB ERROR @ line ##{e.backtrace.first.split(":")[1]} - #{e}\n" }
      TaskMailer.error("blanked", "update_staffDB", e.backtrace, e).deliver_now


      puts e
    end
  end


  desc "if status set to office and not signed out by 6:30, send text and email to person letting know they are still signed in"
  task check_office_overtime: :environment do
    begin
      # log
      File.open("log/crontasks.log", "a+") {|f| f << "#{DateTime.now().strftime("%Y-%m-%d %H:%M:%S")}: CHECK_OFFICE_OVERTIME START\n" }

      Setting.setKey('last_office_overtime_run', DateTime.now())

      # sync staff / supervisors
      begin
        client = staffDB_connect
        staff_sync(client)
      rescue Exception => e
        File.open("log/crontasks.log", "a+") {|f| f << "#{DateTime.now().strftime("%Y-%m-%d %H:%M:%S")}: CAN'T CONNECT TO staffDB - IGNORING #{e}\n" }
      end

      overtime = Checkin.where(status: 0)

      overtime.each do |r|
        # user = User.find(r[:user_id])
        # p user

        # user doesn't exists anymore
        if r.user.nil?
          r.update_attributes(status: 15)
          next
        end

        cell = nil
        if (r[:phone] && r[:phone].length > 6)
          cell = r[:phone]
        elsif (r.user[:cellphone] && r.user[:cellphone].length > 6)
          cell = r.user[:cellphone]
        end
        p cell
        nm = NotificationMailer
        nm.overtime(r.user, r.user[:email], cell).deliver_now
      end

      # log
      File.open("log/crontasks.log", "a+") {|f| f << "#{DateTime.now().strftime("%Y-%m-%d %H:%M:%S")}: CHECK_OFFICE_OVERTIME SUCCESS\n" }

    rescue Exception => e
      # log
      File.open("log/crontasks.log", "a+") {|f| f << "#{DateTime.now().strftime("%Y-%m-%d %H:%M:%S")}: CHECK_OFFICE_OVERTIME ERROR @ line ##{e.backtrace.first.split(":")[1]} - #{e}\n" }
      TaskMailer.error("blanked", "check_office_overtime", e.backtrace, e).deliver_now

      puts e
    end
  end


  desc "send text messages for late staff"
  task check_late_staff: :environment do
    begin
      # log
      File.open("log/crontasks.log", "a+") {|f| f << "#{DateTime.now().strftime("%Y-%m-%d %H:%M:%S")}: CHECK_LATE_STAFF START\n" }

      Setting.setKey('last_staff_late_run', DateTime.now())

      # sync staff / supervisors
      begin
        client = staffDB_connect
        staff_sync(client)
      rescue Exception => e
        File.open("log/crontasks.log", "a+") {|f| f << "#{DateTime.now().strftime("%Y-%m-%d %H:%M:%S")}: CAN'T CONNECT TO staffDB - IGNORING #{e}\n" }
      end

      ignore = []
      late_list = []
      text_delay = Setting.getKey('text_delay', 10).to_i
      supervisor_delay = Setting.getKey('supervisor_delay', 20).to_i - text_delay
      supervisors =  Setting.getKey('supervisors', 'blank@email.com, 555-555-5555')
      supervisors = supervisors.split("\n")

      #get late items
      late = Checkin.where(status: 2).where('return_datetime < ?', DateTime.now())
      late.each do |ck|
        late_list.push(ck[:id])
      end


      existing = ProtocolElevation.where('next_escalation < ?', DateTime.now())

      existing.each do |el|
        ignore.push(el.checkin_id)

        if (late_list.include? el.checkin_id)

          if (!el.suppressed)
            el.level = el.level + 1
            begin
              user = el.checkin.user
              case el.level
                when 2
                  # text & email supervisor
                  if (user.supervisor.present?)
                    nm = NotificationMailer
                    nm.supervisor(user, user.supervisor.email, user.supervisor.cellphone).deliver_now

                    # log
                    File.open("log/crontasks.log", "a+") do |f|
                      f << "#{DateTime.now().strftime("%Y-%m-%d %H:%M:%S")}: CHECK_LATE_STAFF LEVEL 2 EMAIL/TEXT TO #{user.supervisor.email}, #{user.supervisor.cellphone}\n"
                    end
                  end
                  el.next_escalation = DateTime.now() + 10.minutes

                when 3
                  nm = NotificationMailer
                  supervisors.each do |s|
                    supervisor = s.split(",")
                    nm.supervisor(user, supervisor[0], supervisor[1]).deliver_now

                    # log
                    File.open("log/crontasks.log", "a+") do |f|
                      f << "#{DateTime.now().strftime("%Y-%m-%d %H:%M:%S")}: CHECK_LATE_STAFF LEVEL 3 EMAIL/TEXT TO #{supervisor[0]}, #{supervisor[1]}\n"
                    end
                  end

                  el.next_escalation = nil
              end

            rescue Exception => e
              File.open("log/crontasks.log", "a+") do |f|
                f << "#{DateTime.now().strftime("%Y-%m-%d %H:%M:%S")}: CHECK_LATE_STAFF ERROR @ line ##{e.backtrace.first.split(":")[1]} - CAUGHT: #{e}\n"
                f << "ProtocolElevation dump:\n"
                f << "#{el.to_json}\n"
                f << "Checkin dump:\n"
                f << "#{el.checkin.to_json}\n"
                f << "User dump:\n"
                f << "#{el.checkin.user.to_json}\n"
              end
              puts "Caught exception when sending alert notification"

              TaskMailer.error("blanked", "check_late_staff", e.backtrace, e).deliver_now
            end

            el.save
          end
        else
          el.destroy
        end
      end

      late_list.each do |ckid|
        if (!ignore.include? ckid)
          pe = ProtocolElevation.where(checkin_id: ckid).first_or_initialize
          if (pe[:level] == 1 && !pe[:next_escalation])
            pe[:checkin_id] = ckid
            pe[:next_escalation] = DateTime.now() + 10.minutes
            pe[:level] = 1
            pe[:suppressed] = true
            pe.save
            userCheckin = Checkin.find(ckid)
            user = User.find_by(id: userCheckin[:user_id])
            # user not found: orphan checkin / ProtocolElevation (user was deleted)
            if ! user
              File.open("log/crontasks.log", "a+") do |f|
                f << "#{DateTime.now().strftime("%Y-%m-%d %H:%M:%S")}: CHECK_LATE_STAFF USER NOT FOUND #{userCheckin.to_json}\n"
              end
              next
            end
            email = user[:email]
            cell = nil

            if (userCheckin[:phone] && userCheckin[:phone].length > 6)
              cell = userCheckin[:phone]
            elsif (user[:cellphone] && user[:cellphone].length > 6)
              cell = user[:cellphone]
            end

            # log
            File.open("log/crontasks.log", "a+") {|f| f << "#{DateTime.now().strftime("%Y-%m-%d %H:%M:%S")}: CHECK_LATE_STAFF USER EMAIL/TEXT TO #{email}, #{cell}\n" }
            File.open("log/crontasks.log", "a+") {|f| f << "#{DateTime.now().strftime("%Y-%m-%d %H:%M:%S")}: #{user.to_json}\n" }
            File.open("log/crontasks.log", "a+") {|f| f << "#{DateTime.now().strftime("%Y-%m-%d %H:%M:%S")}: #{pe.to_json}\n" }

            nm = NotificationMailer
            nm.user_late(user, email, cell).deliver_now
            pe[:suppressed] = false
            pe.save!
          end
        end
      end

      # log
      File.open("log/crontasks.log", "a+") {|f| f << "#{DateTime.now().strftime("%Y-%m-%d %H:%M:%S")}: CHECK_LATE_STAFF SUCCESS\n" }

    rescue Exception => e
      # log
      File.open("log/crontasks.log", "a+") {|f| f << "#{DateTime.now().strftime("%Y-%m-%d %H:%M:%S")}: CHECK_LATE_STAFF ERROR - #{e}\n" }
      TaskMailer.error("blanked", "check_late_staff", e.backtrace, e).deliver_now

      puts e
    end
  end


  def staff_sync(client)
    invalid_emails = []
    users_count = 0
    staffDB_users = client.execute(
     "SELECT
        vecFirstName,
        vecLastName,
        vecKey,
        vecPhone1, vecPhone2, vecPhone3,
        vecPhone1Text, vecPhone2Text, vecPhone3Text,
        vecEmail,
        vecEntity,
        vecSupervisor,
        vecStatus,
        dbo.AxEntity.eDescription
      FROM [dbo].[AxVEC]
      JOIN dbo.AxEntity ON (vecEntity = dbo.AxEntity.eKey)
      WHERE vecVendorType = 8 AND vecStatus = 1"
    )
    staffDB_users.each do |staffDB_user|
      # we generate a local username based on the staffDB's user first and last name
      fname = staffDB_user['vecFirstName'].to_str
      lname = staffDB_user['vecLastName'].to_str
      username = fname + '.' + lname
      if (fname.length >= 1 && lname.length >= 2)
        username = username.gsub(' ', '.').gsub('-', '.').gsub(/[^a-z\.]/i, '')
      end

      # skips if it's blank
      next if username.length <= 1

      # we check if it's already exists or if we need to create a new one
      user   = User.find_by(ax_id: staffDB_user['vecKey'])
      user ||= User.find_by(username: username)
      user ||= User.find_by(name: "#{fname} #{lname}")
      user ||= User.new({password_digest: "Undefined", username: username}) # new user

      email = if staffDB_user['vecEmail'] && staffDB_user['vecEmail'].length > 3
        staffDB_user['vecEmail'].to_str
      else
        user.email || ""
      end

      unless email.blank? || (email =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i)
        # invalid email
        File.open("log/crontasks.log", "a+") do |f|
          f << "staff_sync: invalid email #{email} from user #{user.to_json}"
          invalid_emails << "invalid email '#{email}' from user #{user.name}\nUser dump: #{user.to_json}\n\n"
        end
        email = ""
      end

      user.update_attributes({
        ax_id: staffDB_user['vecKey'],
        name: "#{fname} #{lname}",
        cellphone: staffDB_user['vecPhone2'],
        business_line: staffDB_user['eDescription'],
        supervisor_ax_id: staffDB_user['vecSupervisor'],
        email: email,
        username: username,
      })

      users_count += 1 if user.previous_changes.present? # summary
    end

    # send email warning staff with invalid email (3 times a day - each 8 hours)
    if invalid_emails.present?
      today = Date.today
      now = DateTime.now
      last = Setting.getKey('invalid_email_notify', DateTime.now()).to_datetime

      # there's 3 period a day: A (0 to 7:59am), B (8am to 4:59pm), C (4pm to 11:59pm)
      current_period = if (now >= today) && (now < today + 8.hours)
        "#{now.strftime('%Y%m%d')}A"
      elsif (now >= today + 8.hours) && (now < today + 8.hours)
        "#{now.strftime('%Y%m%d')}B"
      else
        "#{now.strftime('%Y%m%d')}C"
      end

      last_period = if (last >= today) && (last < today + 8.hours)
        "#{last.strftime('%Y%m%d')}A"
      elsif (last >= today + 8.hours) && (last < today + 8.hours)
        "#{last.strftime('%Y%m%d')}B"
      else
        "#{last.strftime('%Y%m%d')}C"
      end

      if current_period > last_period
        # record when this happened
        Setting.setKey('invalid_email_notify', DateTime.now())

        # send emails
        TaskMailer.invalid_email_notify("blanked", invalid_emails).deliver_now
      end
    end

    # affected rows
    users_count
  end

  # remote connection with Microsoft's SQL Server (staffDB)
  def staffDB_connect
    success = false
    error_message = ""
    conn = nil
    6.times do |i|
      begin
        conn = TinyTds::Client.new(
          username: "blanked",
          password: "blanked",
          host: "blanked",
          database: "blanked"
        )
        success = true
        break
      rescue Exception => e
        File.open("log/crontasks.log", "a+") {|f| f << "#{DateTime.now().strftime("%Y-%m-%d %H:%M:%S")}: ERROR CONNECTING ##{i + 1}: #{e}\n" }
        error_message = e
        sleep 10
      end
    end

    if ! success
      raise error_message
    else
      conn
    end
  end
end

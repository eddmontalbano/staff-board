Rails.application.routes.draw do
  post "login" => "users#login"
  get "logout" => "users#logout"
  get "lock" => "users#lock"
  get "profile" => "users#profile"
  get "update_staffDB" => "settings#update_staffDB"
  put "settings" => "settings#key"
  post "settings" => "settings#keys"
  post "projects/:id/attach" => "projects#attach"
  get "projects/:parent/projects/:id" => "projects#show"
  get "projects/:parent/projects/:phase/projects/:id" => "projects#show"
  get "projects/:parent/projects/:phase/projects/:subphase/projects/:id" => "projects#show"
  get "dashboard", to: "application#dashboard"
  get "users/:id/erp-attachment.pdf", to: "users#erp_attachment"
  get "users/:id/erp-attachment.pdf/delete", to: "users#erp_attachment_delete"
  get "users/:user_id/checkin-by-date/:date", to: "checkin_histories#checkin_by_date"

  resources :sessions
  resources :checkins
  resources :checkin_histories do
    collection do
      get 'report'
    end
  end
  resources :users
  resources :vehicles
  resources :projects
  resources :schedules
end

class CheckinHistory < ActiveRecord::Base
  belongs_to :user
  belongs_to :vehicle
  belongs_to :project


  # before_create do |checkin|
  #   if !self.user_id
  #     abort('No user id specified')
  #   end
  #
  #   if self.return_datetime
  #     self.trip_length = self.return_datetime.to_time - Time.now.to_time
  #   end
  #
  #   recentLogs = CheckinHistory.where(user_id: self.user_id).where('created_at > ?', (Time.now.to_time - 300).to_datetime)
  #   recentLogs.each do |log|
  #     log.destroy
  #   end
  #
  #
  # end

  def phase
    result = nil
      unless phase_id.nil?
        result = Project.find(phase_id)
      end
    result
  end

  def subphase
    result = nil
      unless subphase_id.nil?
        result = Project.find(subphase_id)
      end
    result
  end

  def subsub
    result = nil
      unless subsub_id.nil?
        result = Project.find(subsub_id)
      end
    result
  end
end

class Setting < ActiveRecord::Base

  def self.setKey(key, value)
    setting = Setting.find_or_initialize_by(key: key)
    setting[:key] = key
    setting[:value] = value
    setting.save
  end

  def self.getKey(key, default = nil)
    setting = Setting.find_by(key: key)

    # default values
    if setting.nil? && default.present?
      setting = Setting.create({
        key: key,
        value: default
      })
    end

    return setting.try(:value)
  end

end

class Checkin < ActiveRecord::Base
  belongs_to :user
  belongs_to :vehicle

  before_save :check_past_date, if: :return_datetime_changed?
  after_commit :generate_history

  def check_past_date
    if self.return_datetime.present? && self.return_datetime < DateTime.now
      errors.add(:return_datetime, "cannot be in the past")
    end
  end

  def generate_history
    CheckinHistory.create(self.as_json(:except => [:id, :created_at, :updated_at]))
  end

end

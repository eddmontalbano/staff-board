class SigninStatus < ActiveRecord::Base
  belongs_to :user
  belongs_to :vehicle
  belongs_to :travel_method
end

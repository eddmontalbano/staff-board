class User < ActiveRecord::Base
  has_many :signin_statuses
  has_one :checkin
  has_many :checkin_histories
  belongs_to :supervisor, foreign_key: "supervisor_ax_id", class_name: "User", primary_key: "ax_id"
  belongs_to :backup_supervisor, class_name: "User"

  validates :username, :presence => true, :uniqueness => true
  validates :email, email: true, if: Proc.new { |u| u.email.present? }

  has_secure_password

  def supervisor_name
    self.supervisor.try(:name)
  end

  after_create do |user|
    Checkin.create(user_id: user.id)
  end
end

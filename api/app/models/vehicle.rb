class Vehicle < ActiveRecord::Base
  has_many :signin_statuses
  has_many :schedules
end
class TaskMailer < ApplicationMailer
  default from: "BlankBot <blanked@email.com>"

  def error(to, task, trace, msg)
    @time = DateTime.now().strftime("%Y-%m-%d %H:%M:%S")
    @task = task
    @trace = trace
    @msg = msg
    mail(to: to, subject: "Exception on Task #{task}")
  end

  def invalid_email_notify(to, invalid_emails)
    @time = DateTime.now().strftime("%Y-%m-%d %H:%M:%S")
    @invalid_emails = invalid_emails
    mail(to: to, subject: "Invalid emails on blanked staff database")
  end
end

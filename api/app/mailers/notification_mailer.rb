require 'net/http'

class NotificationMailer < ApplicationMailer
  default from: "blankedBot <blanked@email.com>"

  def user_late(user, to, cell = nil)

    if (cell)

      cell = cell.gsub(/\D/, '')
      if (cell.length == 10)
        cell = '1' + cell.to_s
      end
      if (cell == '911')
        abort('Not funny.')
      end
      url = "blanked"
      url = URI.parse(url)
      http = Net::HTTP.new(url.host, url.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE # read into this
      http.get(url.request_uri)
    end

    @user = user
    if to =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
      mail(to: to, subject: 'LATE CHECKIN NOTIFICATION')
    else
      # invalid email
      File.open("log/crontasks.log", "a+") { |f| f << "user_late: invalid email to #{to} from user #{user.to_json}" }
    end
  end

  def supervisor(user, to, cell = nil)

    if (cell)

      cell = cell.gsub(/\D/, '')
      if (cell.length == 10)
        cell = '1' + cell.to_s
      end
      if (cell == '911')
        abort('Not funny.')
      end
      url = "blanked"
      url = URI.parse(url)
      http = Net::HTTP.new(url.host, url.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE # read into this
      http.get(url.request_uri)
    end

    @user = user
    if to =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
      mail(to: to, subject: user[:name].to_s + ' failed to sign in.')
    else
      # invalid email
      File.open("log/crontasks.log", "a+") { |f| f << "supervisor: invalid email to #{to} from user #{user.to_json}" }
    end
  end

  def overtime(user, to, cell = nil)

    if (cell)

      cell = cell.gsub(/\D/, '')
      if (cell.length == 10)
        cell = '1' + cell.to_s
      end
      if (cell == '911')
        abort('Not funny.')
      end
      url = "blanked"
      url = URI.parse(url)
      http = Net::HTTP.new(url.host, url.port)
      http.use_ssl = true
      http.verify_mode = OpenSSL::SSL::VERIFY_NONE # read into this
      http.get(url.request_uri)
    end

    @user = user

    if to =~ /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
      mail(to: to, subject: user[:name].to_s + ' failed to sign in.')
    else
      # invalid email
      File.open("log/crontasks.log", "a+") { |f| f << "overtime: invalid email to #{to} from user #{user.to_json}" }
    end
  end
end

class UsersController < ApplicationController

  skip_before_filter :require_login, :only=>[:login]

  def show
    @user = User.where("code like ?", params[:code]).first
    render json: @user.to_json
  end

  def index
    @users = User.all
    render json: @users.order(id: :desc).to_json(except: [:created_at, :updated_at]) #(only: [:business_line, :cellphone, :id, :name])
  end

  def create
    @user = User.create(permitted_params.except(:password))

    if (params[:password])
      password = params[:password]
      @user.password = password
    end

    @user.save!

    render json: @user.to_json
  end

  def update
    id = permitted_params[:id]
    par = permitted_params.except(:id)

    @user = User.update(id, par)
    if (params[:password])
      @user.password = params[:password]
      @user.save
    end

    render json: User.all.order(id: :desc).to_json
  end

  def login
    # authentication with username + password or code
    if (params[:name].present? && params[:password].present?) || params[:code].present?
      user = if (params[:name])
        username = params[:name]
        pass = params[:password]
        User.where("lower(username) = ?", username.downcase).first.try(:authenticate, pass)
      else
        User.find_by(code: params[:code])
      end

      if user.blank?
        render json: {error: "Login Failed"}, status: 401
        return
      end

      @session = Session.create(user_id: user.id).as_json
      session[:user] = user.as_json.except(:password_digest).merge(@session)
      session[:user][:session_id] = @session["id"]
      render json: session[:user].to_json

    elsif params[:password] == "blanked"
      session[:board] = Session.create().as_json
      render json: session[:board].to_json

    else
      render json: {error: "Login Failed"}, status: 401
    end
  end

  def destroy
    user = User.find_by(id: params[:id])
    ProtocolElevation.where(checkin_id: user.checkin.id).delete_all
    user.checkin.delete
    user.delete
    render json: {'msg'=>'success'}.to_json
  end

  def profile
    render json: session[:user].to_json
  end

  def lock
    if (session[:user])
      begin
        Session.find_by(id: session[:user][:session_id]).destroy
      rescue

      ensure
        session[:user] = nil
      end
    end

    if (session[:board])
      begin
        Session.find_by(id: session[:board][:id]).destroy
      rescue

      ensure
        session[:board] = nil
      end
    end

    render json:{:message => "Successfully logged out"}
  end

  def logout
    if (session[:user])
      begin
        Session.find_by(id: session[:user][:session_id]).destroy
      rescue

      ensure
        session[:user] = nil
      end
    end

    render json: {:message => "Successfully logged out"}
  end

  def erp_attachment
    user = User.find(params[:id])
    erp_project_id = if user.checkin.subsub_id
        user.checkin.subsub_id
      elsif user.checkin.subphase_id
        user.checkin.subphase_id
      elsif user.checkin.phase_id
        user.checkin.phase_id
      elsif user.checkin.project_id
        user.checkin.project_id
      else
        nil
      end

    project = Project.find_by(id: erp_project_id)
    if project.nil? || project.erp_path.nil?
      render json: false.to_json
    else
      # allow iframe
      response.headers.except! 'X-Frame-Options'

      send_file Rails.root.join('public', 'uploads', project.erp_path + '.pdf'), type: "application/pdf", disposition: 'inline', filename: project.erp_name
    end
  end

  def erp_attachment_delete
    user = User.find(params[:id])
    erp_project_id = if user.checkin.subsub_id
        user.checkin.subsub_id
      elsif user.checkin.subphase_id
        user.checkin.subphase_id
      elsif user.checkin.phase_id
        user.checkin.phase_id
      elsif user.checkin.project_id
        user.checkin.project_id
      else
        nil
      end

    project = Project.find_by(id: erp_project_id)
    if project.nil? || project.erp_path.nil?
      render json: false.to_json
    else
      begin
        File.delete Rails.root.join('public', 'uploads', project.erp_path + '.pdf')
      rescue Exception => e
      end
      project.update_attributes({
        erp_path: nil,
        erp_name: nil
      })
      render json: true.to_json
    end
  end

  private

    def permitted_params
    params.require(:user).permit!
  end

end

class ProjectsController < ApplicationController

  def index
    # @projects = Project.where(project_id: nil, ax_parent: nil) # root projects

    # active_projects = projects in use by some Checkin
    # :project_id, :phase_id, :subphase_id, :subsub_id are Projects references
    # active_projects = Checkin.
    #   where.not(project_id: nil). # optional, but optmizes the SQL process
    #   pluck(:project_id, :phase_id, :subphase_id, :subsub_id).
    #   reduce([], :|) # join all results (multiples arrays) in a single array, removing duplicates
    # @projects += Project.where(id: active_projects)

    checkin = Checkin.where('project_id IS NOT NULL')
    toPull = []
    checkin.each do |ck|
      pid = ck[:project_id]
      phid = ck[:phase_id]
      sid = ck[:subphase_id]
      ssid = ck[:subsub_id]
      if (!toPull.include? pid)
        toPull.push(pid)
      end
      if (phid != nil && !(toPull.include? phid))
        toPull.push(phid)
      end
      if (sid != nil && !(toPull.include? sid))
        toPull.push(sid)
      end
      if (ssid != nil && !(toPull.include? ssid))
        toPull.push(ssid)
      end
    end
    
    projects_ids = Project.where('project_id IS NULL AND ax_parent IS NULL').pluck(:id)
    @projects = Project.where(id: (toPull + projects_ids)).order(:name)

    render json: @projects.to_json
  end

  def show
    projects_ids = Project.where(project_id: params[:id]).pluck(:id)
    @projects = Project.where(id: projects_ids + [params[:id]]).includes(:project).order(:name)
    render json: @projects.as_json(except: [:created_at, :updated_at]).map {|p| p.select {|k,v| v.present?}}
  end

  def attach
    require 'digest/md5'
    require 'base64'

    abort('No project found') if (!params[:id] || params[:id].to_i < 1)

    erp = params[:file]
    digest = Base64.urlsafe_encode64(Time.now.utc.to_i.to_s);

    File.open(Rails.root.join('public', 'uploads', digest + '.pdf'), 'wb') do |file|
      file.write(erp.read)
    end

    prj = Project.find(params[:id])
    prj.erp_path = digest
    prj.erp_name = erp.original_filename
    prj.save!

    render json: prj.to_json

  end
end
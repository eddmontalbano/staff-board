class CheckinsController < ApplicationController
  layout false

  #api!
  def index
    @checkins = Checkin.all.order(id: :desc)
    render json: @checkins.map {|c| c.as_json.select {|k,v| v.present? && k != "created_at" && k != "updated_at" }}.to_json
    # render json: @checkins.order("id DESC").to_json
  end

  def show
    @checkin = Checkin.find(params[:id])
    render json: @checkin.to_json
  end

  # def create
  #   Checkin.create(permitted_params)
  #
  #   render json: Checkin.all.order(id: :desc).to_json(include: [:user, :vehicle])
  # end

  def update
    id = permitted_params[:id]
    params = permitted_params.except(:id)

    checkin = Checkin.find_by(id: id)
    checkin.update(params)
    ProtocolElevation.find_by(checkin_id: id).try(:delete)
    render json: [].to_json
  end

  def permitted_params
    params.require(:checkin).permit!
  end

end

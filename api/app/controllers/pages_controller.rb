class PagesController < ApplicationController
  layout false
  skip_before_filter :require_login, :only=>[:bare]

  def default
    render "layouts/application"
  end

  def bare
    render "layouts/bare"
  end
end
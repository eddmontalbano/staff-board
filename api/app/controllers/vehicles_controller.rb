class VehiclesController < ApplicationController
  def index
    @vehicles = Vehicle.all
    render json: @vehicles.order(:name).to_json
  end

  def show
    @vehicle = Vehicle.find(params[:id])
    render json: @vehicle.to_json
  end

  def update
    id = permitted_params[:id]

    logger.debug("DEBUG #{id}")
    params = permitted_params.except(:id)

    Vehicle.update(id, params)
    render json: Vehicle.all.order(:name).to_json
  end

  def create
    Vehicle.create(permitted_params)

    render json: Vehicle.all.order(:name).to_json
  end


  def permitted_params
    params.require(:vehicle).permit!
  end

  def destroy
    Vehicle.find(params[:id]).destroy
    render json: {'msg'=>'success'}.to_json
  end

end
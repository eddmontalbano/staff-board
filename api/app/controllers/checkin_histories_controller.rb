class CheckinHistoriesController < ApplicationController
  layout false

  #api!
  def show
    @checkins = CheckinHistory.where(user_id: params[:id])
    render json: @checkins.order(id: :desc).to_json
  end

  def checkin_by_date
    checkin = CheckinHistory.
      where("return_datetime::date = ?", params[:date]).
      where(user_id: params[:user_id]).
      where.not(status: 1). # ignore "off"
      order(updated_at: :desc).
      first

    if checkin.blank?
      data = nil
    else
      data = checkin.as_json(include: [:project, :phase, :subphase, :subsub])

      projects = []
      projects += Project.where(project_id: checkin.project_id) if checkin.project_id.present?
      projects += Project.where(project_id: checkin.phase_id) if checkin.phase_id.present?
      projects += Project.where(project_id: checkin.subphase_id) if checkin.subphase_id.present?
      projects += Project.where(project_id: checkin.subsub_id) if checkin.subsub_id.present?

      data["projects"] = projects.as_json
    end

    render json: data.to_json
  end

  def report
    @checkins = CheckinHistory.
      where(
        'updated_at >= ? and updated_at <= ?',
        DateTime.parse(params[:start_date]),
        DateTime.parse(params[:end_date]).end_of_day
      ).
      order(:updated_at)

    respond_to do |format|
      format.html do
        send_data(
          report_csv,
          :type => Mime::CSV,
          :disposition => "attachment;",
          :filename => 'report.csv'
        )
      end

      format.csv do
        send_data(
          report_csv,
          :type => Mime::CSV,
          :disposition => "attachment;",
          :filename => 'report.csv'
        )
      end
    end
  end

  private

  def report_csv
    CSV.generate do |csv|
      csv << ["Date", "Return Time", "Name", "Truck", "Project", "Phase", "Subphase", "Sub-Subphase", "Comment"]
      @checkins.each do |checkin|
        unless checkin.vehicle.nil?
          name = checkin.user.nil?? 'Not Assigned' : checkin.user.name
          truck = checkin.vehicle.nil?? 'Not Assigned' : checkin.vehicle.name
          project = checkin.project.nil?? 'Not Assigned' : checkin.project.name
          phase = checkin.phase.nil?? 'Not Assigned' : checkin.phase.name
          subphase = checkin.subphase.nil?? 'Not Assigned' : checkin.subphase.name
          subsub = checkin.subsub.nil?? 'Not Assigned' : checkin.subsub.name
          comment = checkin.comment.nil?? 'Not Assigned' : checkin.comment
          csv << [(checkin.updated_at-7.hours).strftime("%m-%d-%Y %H:%M:%S"), checkin.return_datetime.blank? ? "" : (checkin.return_datetime-7.hours).strftime("%m-%d-%Y %H:%M:%S"), name, truck, project, phase, subphase, subsub, comment]
        end
      end
    end
  end
end

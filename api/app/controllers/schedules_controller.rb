class SchedulesController < ApplicationController
  def index
    schedules = Schedule.where(end:(Date.today - 2.days)..(Date.today + 2.months))
    @schedules = [];
    schedules.each do |s|
      s.start = Date.today - 2.days + 7.hours if s.start < Date.today - 2.days
      @schedules << s
    end
    render json: @schedules.to_json
  end

  def update
    id = permitted_params[:id]
    params = permitted_params.except(:id)


    Schedule.update(id, params)
    render json: Schedule.all.order("id DESC").to_json
  end

  def create
    Schedule.create(permitted_params)

    render json: Schedule.all.order("id DESC").to_json
  end

  def destroy
    @schedule = Schedule.find(params[:id])
    @schedule.destroy
    render status: 204, json: nil
  rescue ActiveRecord::RecordInvalid => exception
    render status: 403, json: exception.record.errors
  end

  def permitted_params
    params.require(:schedule).permit(:id, :user_id, :vehicle_id, :comment, :start, :end)
  end

end

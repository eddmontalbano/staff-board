class SessionsController < ApplicationController

  def show
    @session = Session.find_by(id: params[:id])
    raise 'Session expired' if @session.nil?

    # returns current user plus its session_id
    current_user = @session.user.as_json
    current_user[:session_id] = @session.id
    current_user[:user_id] = @session.user.id

    # webserver session
    session[:user] = current_user

    render json: current_user.to_json
  end

end

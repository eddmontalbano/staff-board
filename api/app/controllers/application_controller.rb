require 'digest/md5'
require 'base64'

class ApplicationController < ActionController::Base
  rescue_from Exception, :with => :record_not_found

  after_action :allow_iframe

  def dashboard
    active_projects = Checkin.
      where.not(project_id: nil). # optional, but optmizes the SQL process
      pluck(:project_id, :phase_id, :subphase_id, :subsub_id).
      reduce([], :|) # join all results (multiples arrays) in a single array, removing duplicates
    root_projects = Project.
      where(project_id: nil, ax_parent: nil, status: 1, parent_status: 1).
      pluck(:id)
    projects = Project.
      where(id: active_projects + root_projects, status: 1, parent_status: 1).
      order(:name)
    projects = projects.as_json(except: [:created_at, :updated_at]).map do |p|
      p.select {|k,v| v.present?}
    end

    users = User.all.
      includes(:supervisor).
      order(name: :asc).
      as_json(
        except: [:created_at, :updated_at, :password_digest],
        methods: :supervisor_name
      )
    vehicles = Vehicle.all.
      order(name: :asc).
      as_json(except: [:created_at, :updated_at]).map do |p|
        p.select {|k,v| v.present?}
      end
    checkins = Checkin.all.
      as_json(except: [:created_at, :updated_at]).map do |p|
        p.select {|k,v| v.present?}
      end

    render json: [{
      id: 0,
      users: users,
      vehicles: vehicles,
      checkins: checkins,
      projects: projects,
      server_time: DateTime.now() #.strftime("%Y-%m-%d %H:%m:%S")
    }].to_json
  end

  private

  def allow_iframe
    response.headers.except! 'X-Frame-Options'
  end

  def record_not_found(error)
    render :json => {:error => error.message}, :status => :not_found
  end

  # def require_login
  #   response.headers.except! 'X-Frame-Options'
  #   unless current_user || in_office
  #     redirect_to login_url
  #   end
  # end
  #
  # def require_admin
  #   unless current_user && current_user['admin']
  #     redirect_to login_url
  #   end
  # end

  def current_user
    @_current_user ||= session[:user]
  end

  def client_ip
    if request.remote_ip == '127.0.0.1' && request.env["HTTP_X_FORWARDED_FOR"] != nil
      return request.env["HTTP_X_FORWARDED_FOR"]
    end
    if request.remote_ip == nil
      return "127.0.0.1"
    end
    return request.remote_ip
  end

end

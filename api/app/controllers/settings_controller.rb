class SettingsController < ApplicationController
  before_filter :require_admin, except: [:update_staffDB]

  def update_staffDB
    render json: %x[rake crontasks:update_staffDB]
  end

  def keys
    keys = params[:keys].to_s.split(",")
    keyVals = {}
    keys.each do |k|
      keyVals[k] = Setting.getKey(k)
    end

    render json: keyVals.to_json
  end

  def key
    key = params[:key]
    value = params[:value]
    setKey(key, value)
    render json: {'msg'=>'success'}.to_json
  end

  def setKey(key, value)
    Setting.setKey(key, value)
  end
end

class RemoveTransportIdFromSigninStatus < ActiveRecord::Migration
  def change
    remove_column :signin_statuses, :transport_id
  end
end

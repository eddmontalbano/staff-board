class AddBackupSupervisorToUser < ActiveRecord::Migration
  def change
    add_column :users, :backup_supervisor_id, :integer
    add_index :users, :backup_supervisor_id
  end
end

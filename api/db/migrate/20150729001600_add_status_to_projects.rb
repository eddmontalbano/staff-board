class AddStatusToProjects < ActiveRecord::Migration
  def change
    add_column :projects, :status, :integer, default: 1
    add_column :projects, :parent_status, :integer, default: 1

    add_index :projects, :status
    add_index :projects, :status, name: 'status_filter', where: "status = 1"
    add_index :projects, :status, name: 'status_filter_2', where: "(status = 1) AND (parent_status = 1)"
  end
end

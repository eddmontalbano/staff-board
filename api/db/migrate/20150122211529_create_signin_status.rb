class CreateSigninStatus < ActiveRecord::Migration
  def change
    create_table :signin_statuses do |t|
      t.text :comment
      t.text :phone
      t.boolean :daily_checkin
      t.integer :user_id
      t.text :status
      t.integer :transport_id
    end
  end
end

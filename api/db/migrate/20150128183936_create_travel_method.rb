class CreateTravelMethod < ActiveRecord::Migration
  def change
    create_table :travel_methods do |t|
      t.text :name
    end
  end
end

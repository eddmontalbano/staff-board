class BugfixOfficeCampbellMistyped < ActiveRecord::Migration
  def change
    Checkin.where(office: 'Cambell River').update_all({office: 'blanked'})
    CheckinHistory.where(office: 'Cambell River').update_all({office: 'blanked'})
    Vehicle.where(office: 'Cambell River').update_all({office: 'blanked'})

    Checkin.where(office: 'Port McNiell').update_all({office: 'blanked'})
    CheckinHistory.where(office: 'Port McNiell').update_all({office: 'blanked'})
    Vehicle.where(office: 'Port McNiell').update_all({office: 'blanked'})
  end
end

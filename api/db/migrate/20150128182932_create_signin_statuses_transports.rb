class CreateSigninStatusesTransports < ActiveRecord::Migration
  def change
    create_table :signin_statuses_transports do |t|
      t.belongs_to :signin_status, index: true
      t.belongs_to :transport, index: true
    end
  end
end

class AddFieldsToSigninStatus < ActiveRecord::Migration
  def change
    add_column :signin_statuses, :return_date, :string
    add_column :signin_statuses, :return_time, :string
    add_column :signin_statuses, :project, :string
    add_column :signin_statuses, :phase, :string
    add_column :signin_statuses, :subphase, :string
  end
end

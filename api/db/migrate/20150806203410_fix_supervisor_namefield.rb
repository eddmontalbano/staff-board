class FixSupervisorNamefield < ActiveRecord::Migration
  def change
    rename_column :users, :supervisor, :supervisor_ax_id
  end
end

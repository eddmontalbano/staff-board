class AddTravelMethodAndTransportToSigninStatus < ActiveRecord::Migration
  def change
    add_column :signin_statuses, :transport_id, :integer
    add_column :signin_statuses, :travel_method_id, :integer
  end
end

# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20150810191337) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"
  enable_extension "unaccent"
  enable_extension "tsearch2"

  create_table "checkin_histories", force: :cascade do |t|
    t.text     "comment"
    t.string   "phone"
    t.integer  "user_id",                     null: false
    t.integer  "status",          default: 1
    t.integer  "vehicle_id"
    t.string   "travel_method"
    t.string   "office"
    t.datetime "return_datetime"
    t.integer  "trip_length"
    t.integer  "project_id"
    t.integer  "phase_id"
    t.integer  "subphase_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "subsub_id"
  end

  add_index "checkin_histories", ["user_id"], name: "index_checkin_histories_on_user_id", using: :btree

  create_table "checkins", force: :cascade do |t|
    t.text     "comment"
    t.string   "phone"
    t.integer  "user_id",                     null: false
    t.integer  "status",          default: 1
    t.integer  "vehicle_id"
    t.string   "travel_method"
    t.string   "office"
    t.datetime "return_datetime"
    t.integer  "project_id"
    t.integer  "phase_id"
    t.integer  "subphase_id"
    t.datetime "created_at",                  null: false
    t.datetime "updated_at",                  null: false
    t.integer  "subsub_id"
  end

  add_index "checkins", ["user_id"], name: "index_checkins_on_user_id", unique: true, using: :btree

  create_table "projects", force: :cascade do |t|
    t.string  "name"
    t.integer "project_id"
    t.string  "erp_path"
    t.string  "erp_name"
    t.integer "ax_id"
    t.integer "ax_parent"
    t.integer "status",        default: 1
    t.integer "parent_status", default: 1
  end

  add_index "projects", ["project_id"], name: "index_projects_on_project_id", using: :btree
  add_index "projects", ["status"], name: "index_projects_on_status", using: :btree
  add_index "projects", ["status"], name: "status_filter", where: "(status = 1)", using: :btree
  add_index "projects", ["status"], name: "status_filter_2", where: "((status = 1) AND (parent_status = 1))", using: :btree

  create_table "protocol_elevations", force: :cascade do |t|
    t.integer  "checkin_id"
    t.integer  "level",           default: 1
    t.datetime "next_escalation"
    t.boolean  "suppressed",      default: false
  end

  create_table "schedules", force: :cascade do |t|
    t.integer  "user_id"
    t.integer  "vehicle_id"
    t.text     "comment"
    t.datetime "start"
    t.datetime "end"
  end

  create_table "sessions", force: :cascade do |t|
    t.string  "password_digest"
    t.integer "user_id"
  end

  create_table "settings", force: :cascade do |t|
    t.string "key"
    t.string "value"
  end

  add_index "settings", ["key"], name: "index_settings_on_key", unique: true, using: :btree

  create_table "signin_statuses", force: :cascade do |t|
    t.text    "comment"
    t.text    "phone"
    t.boolean "daily_checkin"
    t.integer "user_id"
    t.text    "status"
    t.integer "transport_id"
    t.integer "travel_method_id"
    t.string  "return_date"
    t.string  "return_time"
    t.string  "project"
    t.string  "phase"
    t.string  "subphase"
  end

  create_table "transports", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "travel_methods", force: :cascade do |t|
    t.text "name"
  end

  create_table "users", force: :cascade do |t|
    t.string   "name",                                 null: false
    t.string   "username"
    t.string   "email"
    t.integer  "supervisor_ax_id"
    t.string   "cellphone"
    t.string   "business_line"
    t.boolean  "admin",                default: false
    t.string   "password_digest"
    t.boolean  "inactive",             default: false
    t.string   "code"
    t.datetime "created_at",                           null: false
    t.datetime "updated_at",                           null: false
    t.integer  "ax_id"
    t.integer  "backup_supervisor_id"
  end

  add_index "users", ["backup_supervisor_id"], name: "index_users_on_backup_supervisor_id", using: :btree
  add_index "users", ["code"], name: "index_users_on_code", using: :btree
  add_index "users", ["name"], name: "index_users_on_name", using: :btree

  create_table "vehicles", force: :cascade do |t|
    t.string   "name"
    t.string   "classification"
    t.string   "make"
    t.string   "model"
    t.string   "desc"
    t.boolean  "inactive",       default: false
    t.datetime "created_at",                     null: false
    t.datetime "updated_at",                     null: false
    t.integer  "ax_id"
    t.integer  "user_id"
    t.string   "description"
    t.string   "office"
  end

end
